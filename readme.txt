This repository contains the code implemented for our master thesis
"Solving problems by taking pictures - Study of general approaches"

The directory 'images' contains image samples usefull to run the code on.
The directory 'code' contains 4 directories of the different implementations : 
	=> 'coordDependentSearch' is the more interesting folder and contains the Python coordinates dependent implementation
		The main file to execute is 'coordDependentSearch.py'.  Help about of to use it can be found by running 'python coordDependentSearch.py help'

	=> 'coordIndependentSearch' contains the Python coordinates independent implementation
	
	=> 'cpApproach' contains the Scala constraint programming approach
	
	=> 'problemDependentApproach' contains the Java problem dependent approach