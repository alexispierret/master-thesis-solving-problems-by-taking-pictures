 ==================
|INSTALLATION NOTES|
 ==================

 In order to be able to run the given code, the different library depencies must be installed : 

 openCV3 for python and java : 		On OS X, Homebrew can be used to install it
 networkx : 						the pip tool can install it
 matplotlib : 						the pip tool can install it
 python 2.7
 Other python dependencies : 		pip tool should be able to install them