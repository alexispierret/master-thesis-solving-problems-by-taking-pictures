import math
import networkx as nx
from math import *
from thinning import guo_hall_thinning as ght
from collections import defaultdict
from utils import dist
import numpy as np
import cv2
"""
This function performs a thinning on the source image -src
and return the result.  A thinning aims at shrinking every
strokes on the image to be 1 pixel thick only.
"""
def guo_hall_thinning(src):
	return ght(src.copy())

"""
Given a skeleton -skel with strokes 1-pixel thick only, 
this function will detects pixels that can be considered
as nodes.  It returns a networkx Graph containing the nodes.
"""
def node_detection(skel):
	def is_node_1_neighborhood(x, y, skel):
		accept_pixel_as_node = False
		item = skel.item
		p2 = item(x - 1, y) / 255
		p3 = item(x - 1, y + 1) / 255
		p4 = item(x, y + 1) / 255
		p5 = item(x + 1, y + 1) / 255
		p6 = item(x + 1, y) / 255
		p7 = item(x + 1, y - 1) / 255
		p8 = item(x, y - 1) / 255
		p9 = item(x - 1, y - 1) / 255

		A = (p2 == 0 and p3 == 1) + (p3 == 0 and p4 == 1) + \
			(p4 == 0 and p5 == 1) + (p5 == 0 and p6 == 1) + \
			(p6 == 0 and p7 == 1) + (p7 == 0 and p8 == 1) + \
			(p8 == 0 and p9 == 1) + (p9 == 0 and p2 == 1)

		if (A >= 3) or (A == 1):
			accept_pixel_as_node = True
		return accept_pixel_as_node


	graph = nx.Graph()
	h, w = skel.shape
	item = skel.item

	for x in xrange(1,w-2):
		for y in xrange(1,h-2):
			if item(y, x) != 0 and is_node_1_neighborhood(y, x, skel):
				graph.add_node((x, y), level=1)
	return graph



"""
Given a skeleton -skel with strokes 1-pixel thick only and
a graph -graph containing some nodes, this function
will detects the path between the nodes and in that way
adds edges to the graph and degree 2 nodes.
"""
def edge_detection(skel, graph):
	#yields on the skeleton the 1-neighborhood of a pixel
	def neighbors((x, y)):
		item = skel.item
		height, width = skel.shape
		for dy in [-1, 0, 1]:
			for dx in [-1, 0, 1]:
				if (dx != 0 or dy != 0) and 0 <= x + dx < width and 0 <= y + dy < height and item(y + dy, x + dx) != 0:
					yield x + dx, y + dy


	#initialization of the BFSs : 
	label = 1 
	label_node = dict() #map to each label the starting node

	queues = []
	sqrt2 = sqrt(2)
	edges = set()
	label_pixels = defaultdict(list)
	label_length = defaultdict(int)
	pixel_label = defaultdict(int) 
	for x, y in graph.nodes_iter():
		pixel_label[(x,y)]=-1
		for a, b in neighbors((x, y)):
			if pixel_label[(a,b)] == -1:
				graph.add_edge((x,y), (a,b))
			else:
				label_node[label] = (x, y)
				label_length[label] = sqrt2 if abs(x - a) == 1 and abs(y - b) == 1 else 1
				#append a new BFS search for the node x,y starting at a,b
				label_pixels[label].append((x,y))
				queues.append((label, (x, y), [(a, b)]))
			label += 1

	
	#perform all the searches
	while queues:
		new_queues = []

		for label, (px, py), nbs in queues:
			#search with label onto neighbors nbs
			for (nx, ny) in nbs:
				#label of the node	
				plabel = pixel_label[(nx, ny)]
				#if not visited yet
				if plabel == 0:
					label_length[label] += sqrt2 if abs(nx - px) == 1 and abs(ny - py) == 1 else 1
					pixel_label[(nx, ny)] = label
					label_pixels[label].append((nx,ny))
					#start a new searches from the newly labeled node onto its neighbors
					new_queues.append((label, (nx, ny),  [x for x in neighbors((nx, ny)) if pixel_label[x] != -1 and x not in graph]))
				
				#if node visited by another BFS => edge found
				elif plabel != label:
					if plabel != -1:
						edges.add((min(label, plabel), max(label, plabel)))
		queues = new_queues

	for l1, l2 in edges:
		u, v = label_node[l1], label_node[l2]
		if u != v:
			#continuity of the contours
			if ((label_pixels[l2]))[-1] in neighbors(((label_pixels[l1])[-1])):
				length = label_length[l1] + label_length[l2]
				new = list(label_pixels[l2])
				new.reverse()
				pixels = np.array((label_pixels[l1]+ new))
				res = cv2.approxPolyDP(pixels, 0.1*length, False)
				last = (res[0][0][0], res[0][0][1])
				for node in res[1:]:
					newNode = (node[0][0], node[0][1])
					if newNode not in graph:
						graph.add_node(newNode, level=2)
					graph.add_edge(newNode, last)
					last = newNode
			else:
				graph.add_edge(u, v)
	return graph



"""
Given a detected graph -graph and a tolerance of merging
-mergeDist, this function will analyse the -graph and merges 
together the nodes that are too close according the tolerance
"""
def merge_close_node_filter(graph, mergeDist):
	to_remove = set()
	for n1, attr1 in graph.nodes_iter(data=True):
		if n1 in to_remove:#node that is removed
			continue
		for n2 in [x for x in graph.nodes_iter() if dist(x,n1)<mergeDist and n1 != x]:
			graph.add_node(n1,{'level':3})
			for u,v in graph.edges(n2):
				if v != n1:
					graph.add_edge(n1,v)
					to_remove.add(n2)

	print("merge_close_node_filter : "+str(len(to_remove))+" nodes removed")
	graph.remove_nodes_from(to_remove)
	
	return graph


"""
The graph -graph in argument may contains a lot of connected 
components.  Some of them may be very small and considered as 
noise according a minimum number of nodes -minNode.  This function
will filter out every connected components that are too small.
"""
def connected_components_filter(graph, minNode):
    connected_components = sorted(list(nx.connected_component_subgraphs(graph.to_undirected())), key = lambda graph: graph.number_of_nodes())
    for subgraph in connected_components:
        if subgraph.number_of_nodes() < minNode:
        	graph.remove_nodes_from(subgraph)
    return graph