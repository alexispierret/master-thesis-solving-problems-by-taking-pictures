# -*- coding: utf-8 -*-
import cv2 as cv
import math
import numpy as np

"""
Given a -resolution, this function resize the input -img
keeping the ratio widht/height.  The default interpolation scheme
used is INTER_AREA, a good idea when shrinking images to a smaller size
"""
def resize(img, resolution, inter=cv.INTER_AREA):
	if len(img.shape)==2:
		width,height = img.shape
	elif len(img.shape)==3:
		width,height,_ = img.shape

	ratio = img.size / resolution
	return cv.resize(img, (int(math.floor(height/math.sqrt(ratio))), int(math.floor(width/math.sqrt(ratio)))), interpolation=inter)


"""
Given an image -img in input, this function applies a bilateral filter 
on it.  It aims at reducing the noise in the image.
"""
def bilateral_filter(img, diameter, sigma_color, sigma_space):
	return cv.bilateralFilter(img, diameter, sigma_color, sigma_space)

"""
Given an image -img in input, this function will
    1) Turn it in grayscale
    2) Apply a adaptive threshold using the mean scheme
    and the parameters -block_size and -constant in argument
"""
def adaptive_threshold_mean(img, block_size, constant):
    gray = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
    result = cv.adaptiveThreshold(gray, 255, cv.THRESH_BINARY,
        cv.ADAPTIVE_THRESH_MEAN_C, block_size, constant)
    return 255-result

"""
Given an image -img in input, this function will
    1) Turn it in grayscale
    2) Apply a adaptive threshold using the gaussian scheme
    and the parameters -block_size and -constant in argument
"""
def adaptive_threshold_gaussian(img, block_size, constant):
    gray = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
    thresh = cv.adaptiveThreshold(gray, 255, cv.THRESH_BINARY, cv.ADAPTIVE_THRESH_GAUSSIAN_C, block_size, constant)
    return thresh


"""
Given a thresholded image -thresh in input, this function
performs a dilatation and an erosion of the lines found on the 
image.  The kernel sizes can be passed in argument.
The aim of this function is to reconstruct some discontinued 
lines that should be continue.
"""
def dilateErode(thresh, dilateKernelSize=4, erodeKernelSize=3) :        
    k = np.ones((dilateKernelSize,dilateKernelSize),np.uint8)
    thresh2 = cv.dilate(thresh,k,iterations = 1)
    k = np.ones((erodeKernelSize,erodeKernelSize),np.uint8)
    thresh = cv.erode(thresh2,k,iterations = 1)
    return thresh