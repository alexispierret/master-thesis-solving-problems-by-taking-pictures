import sys
import cv2
import math

import preprocessing
import graphDetection
import utils

import models
import graphMatching

import ocr

import pprint as pp
import networkx as nx

# ======PARAMETERS DEFINITION======
resize_size, blur1_k_size, blur2_k_size = 1000000, 3, 3
dnoise_strength, dnoise_twin_size, dnoise_swin_size = 8, 5, 5
thr_block_size, thr_cst = 5, 3
thinning_mode = 0
show, saveResult = True, False
# ======END OF PARAMETERS DEFINITION======


"""
This function contains the whole logic of the coordinates 
independant search.  It takes a -problemName in argument, for 
example 'sudoku' and a list of images on which
to perform the search.  The main steps of the algorithm are 
	STEP 1 - Preprocessing image
	STEP 2 - Graph detection
	STEP 3 - Coordinates Dependant Search
	STEP 4 - Display matching
"""

def coordInddependantSearch(problemName, images=['curved1']) : 
	
	for imgName in images : 
		print "Processing image", imgName
		imgPath = "../../images/" + imgName + ".jpg"
		
		#Loading image
		img = cv2.imread(imgPath)
		if img is None : 
			print "Wrong image name"
			sys.exit()

		#STEP 1 - Preprocessing image
		print "\t Preprocessing the image"
		resized			=	preprocessing.resize(img, resize_size)
		# biFiltered 		=	preprocessing.bilateral_filter(resized, -1,  50, 5)
		thresh 			= 	preprocessing.adaptive_threshold_gaussian(resized, 19, 10)
		eroded 			= 	preprocessing.dilateErode(thresh)

		#STEP 2 - Graph detection
		print "\t Graph detection inside of the image"
		thin 			= 	graphDetection.guo_hall_thinning(eroded)
		sGraph 			=	graphDetection.node_detection(thin)
		sGraph 			= 	graphDetection.edge_detection(thin, sGraph)
		sGraph 			= 	graphDetection.merge_close_node_filter(sGraph, 3)
		sGraph 			= 	graphDetection.connected_components_filter(sGraph, 8)

		#STEP 3 - Coordinates independant Search
		print "\t Starting the coordinates dependant search"
		mGraph 			= 	models.sudokuModel(n=9)
		matching		=	graphMatching.search(mGraph, sGraph, 0.5)

		#STEP 4 - Display matching
		print "\t Display the best matching found"
		imgSGraph 		= 	utils.draw_graph(biFiltered.copy(), sGraph)
		utils.showMatching(mGraph, matching, imgSGraph)


		#In boolean are true, display and/or save intermediate images
		if saveResult:
			utils.saveResult(resized, biFiltered, thresh, eroded, thin, imgName)
		if show:
			utils.draw_graph(resized, sGraph, nodeThick=1, edgeThick=1)



if __name__ == "__main__":
	n = len(sys.argv)
	if n == 1 : 
		print(	"Number of args mismatched\n" +
				" ==============\n" +
				"| Usage notice |\n" +
				" ==============\n" +
				"  arg-1 : \t\tproblem type ('sudoku' or 'mathPyramid')\n" +
				"  [arg-2 -> arg-n] :\ta list of images names where to perform the search\n" + 
				"\t\t\tif not supplied, a default image is used" 
			)
	elif n == 2 : 
		coordIndependantSearch(sys.argv[1])
	else : 
		coordIndependantSearch(sys.argv[1], sys.argv[2:])


