import networkx as nx
from math import *
from collections import defaultdict
import models
import time as t
import operator
import utils
import sys

G_rho = 0.25
"""
This is the main search function.
Given two graph structures -mGRaph and -sGraph, the function
tries to find the best approximate subgraph matching between the
two graphs. The -rho parameters set the degree of approximation 
allowed.
"""
def search(mGraph, sGraph, rho=0.1):
	global G_rho
	G_rho = rho
	startGen = startingMatch(mGraph, sGraph)
	queue = next(startGen)
	best = {}
	bestVal = 0
	allmatches = []
	value = -1
	match = defaultdict(list)
	i = 0
	nobetter = 0

	#stop condition for the search : 3 consecutive match were not better that the best one found so far
	while i < 100 and nobetter < 3:
		match, value = extendMatch(mGraph, sGraph, queue)
		if bestVal < value:
			nobetter = 0
			best = match
			bestVal = value
		else:
			nobetter +=1
		queue = next(startGen, None)
		if not queue:
			break
		i+=1
	
	return best

"""
Yield the sorted basic matchings of nodes of twos graphs.
The ordering rely on the definition of kinds and of the 
evaluateMatching procedure
"""
def startingMatch(mGraph, sGraph):
	#step1 : sort model by kind
	mKinds = defaultdict(list)
	sKinds = defaultdict(list)
	storedValue = []

	for mNode in mGraph.nodes_iter():
		kind = getKind(mGraph, mNode)
		mKinds[kind].append(mNode)

	#step2 : mesure distance from kinds with space nodes
	for kind in mKinds.keys(): 
		mNode = mKinds[kind][0]
		for sNode in sGraph.nodes_iter():
			match, w = evaluateMatching(mGraph, mNode, sGraph, sNode)
			if match:
				sKinds[kind].append((sNode, w))
				storedValue.append((sNode, kind, w))
	
	#step3 ordering of the yield
	for kind in sorted(mKinds.keys(), key = lambda k: (len(mKinds[k]),len(sKinds[k])), reverse = False):
		for mNode in [mKinds[kind][0]]:
			for sNode, w in sorted(sKinds[kind], key = lambda k : k[1], reverse=True):
				yield [(mNode,sNode,w)]
	
"""
Kind computation policy
"""
def getKind(graph, node):
	return graph.degree(node), nbConnection(graph, node), nb2Connection(graph, node)



"""
From a starting set of match -startingMatch, this function extends the
match in order to form the final graph match between -mGraph and -sGraph
"""
def extendMatch(mGraph, sGraph, startingMatch):
	queue = startingMatch
	i = 0
	match = {}
	value = 0
	stall = 0
	while len(queue)>0 and i < 500:
		queue = sorted(queue, key = lambda k : (k[2], rightNeigh(mGraph, sGraph, k[0], k[1], match),right2Neigh(mGraph, sGraph, k[0], k[1], match), k[1]), reverse=True)

		(mNode, sNode, w) = queue.pop(0)
		examineNearbyNodes(mGraph, sGraph, mNode, sNode, match, queue)
		match[mNode] = (sNode, w, i)
		i+=1
	for mNode in match.keys():
		value+=rightNeigh(mGraph, sGraph, mNode, match[mNode][0], match)/mGraph.degree(mNode)+right2Neigh(mGraph, sGraph, mNode, match[mNode][0], match)//mGraph.degree(mNode)
	return match, value

"""
This function return the sum of the qualities of the 
matches of the 1-neighbours nodes already in the final match
"""
def rightNeigh(mGraph, sGraph, mNode, sNode, match):
	n=1
	s = 0
	for mNeigh in get1neighbors(mGraph, mNode):
		for sNeigh in get1neighbors(sGraph, sNode):
			if mNeigh in match.keys() and match[mNeigh][0]==sNeigh:
				n+=1
				s+=match[mNeigh][1]
	return s

"""
This function return the sum of the qualities of the 
matches of the 2-neighbours nodes already in the final match
"""
def right2Neigh(mGraph, sGraph, mNode, sNode, match):
	n=1
	s = 0
	for mNeigh in get2neighbors(mGraph, mNode):
		for sNeigh in get2neighbors(sGraph, sNode):
			if mNeigh in match.keys() and match[mNeigh][0]==sNeigh:
				n+=1
				s+=match[mNeigh][1]
	return s

"""
This function examine and add to the queue candidates matches in 
that belongs to the neighborhoods of -mNode and -sNode. The graph 
of the corresponding nodes are -mGraph and -sGraph and the actual
state of the graph match is passed as -match.
"""
def examineNearbyNodes(mGraph, sGraph, mNode, sNode, match, queue):
	nextMNodes = [x for x in get1neighbors(mGraph, mNode) if x not in match.keys()]
	next2MNodes = [x for x in get2neighbors(mGraph, mNode) if x not in match.keys()]
	
	sInQueue = [x[1] for x in queue]
	sInMatch = [x[0] for x in match.values()]
	nextSNodes = [x for x in get1neighbors(sGraph, sNode)  if x not in sInMatch and x not in sInQueue]
	next2SNodes = [x for x in get2neighbors(sGraph, sNode)  if x not in sInMatch and x not in sInQueue]

	matchNodes(mGraph, sGraph, nextMNodes,  nextSNodes,  match, queue)
	matchNodes(mGraph, sGraph, next2MNodes, nextSNodes,  match, queue)
	matchNodes(mGraph, sGraph, nextMNodes,  next2SNodes, match, queue)
	# matchNodes(mGraph, sGraph, next2MNodes,  next2SNodes, match, queue)

"""
This function try to match set of nodes -nextMNodes and -nextSNodes
from two graphs -mGraph and -sGraph. The match are evaluated consistently
with the current matches found so far -match and the candidates match
to be examined -queue.
"""
def matchNodes(mGraph, sGraph, nextMNodes, nextSNodes, match, queue):
	for nextMNode in nextMNodes:
		sCandidate = []
		for nextSNode in nextSNodes:
			m, w = evaluateMatching(mGraph, nextMNode, sGraph, nextSNode)
			if m:
				sCandidate.append((nextSNode, w, rightNeigh(mGraph, sGraph, nextMNode, nextSNode, match), right2Neigh(mGraph, sGraph, nextMNode, nextSNode, match)))

		if len(sCandidate) == 0:
			continue
		
		#two choices can be applied, update all the candidates or only the best one. The first choice is more accurate but take more time to compute
		for (nextSNode, w, rn1, rn2) in sorted(sCandidate, key = lambda k :(k[1],k[2],k[3]), reverse=True):
		# (nextSNode, w, rn1, rn2) = sorted(sCandidate, key = lambda k : (k[1],k[2],k[3]), reverse=True)[0]
			inQueue = [x for x in queue if x[0] == nextMNode]
			if len(inQueue) == 0:
				queue.append((nextMNode, nextSNode, w))
				nextSNodes.remove(nextSNode)
			elif w > inQueue[0][2] or (w == inQueue[0][2] and (rn1 > rightNeigh(mGraph, sGraph, inQueue[0][0], inQueue[0][1], match))):
				queue.remove(inQueue[0])
				queue.append((nextMNode, nextSNode, w))
				nextSNodes.remove(nextSNode)

"""
Evaluate the match and its quality between two nodes belonging to two graphs
"""
def evaluateMatching(mGraph, mNode, sGraph, sNode):
	rho=G_rho
	mDegree = float(mGraph.degree(mNode))
	mConnection = float(nbConnection(mGraph, mNode))
	m2Connection = float(nb2Connection(mGraph, mNode))

	sDegree = float(sGraph.degree(sNode))
	sConnection = float(nbConnection(sGraph, sNode))
	s2Connection = float(nb2Connection(sGraph, sNode))

	#conditions for a match
	nbmiss = mDegree*rho
	nbcmiss = (nbmiss*(nbmiss-1)/2+(mDegree-nbmiss)*nbmiss)

	missDegree = max(mDegree-sDegree, 0)
	missConnection = max(mConnection-sConnection, 0)
	miss2Connection = max(m2Connection-s2Connection, 0)

	overDegree = max(sDegree-mDegree, 0)
	overConnection = max(sConnection-mConnection, 0)
	over2Connection = max(s2Connection-m2Connection, 0)

	degreeMatch = missDegree <= nbmiss
	connectionMatch = missConnection <= nbcmiss
	connection2Match = miss2Connection <= nbcmiss

	has_matched = degreeMatch and connectionMatch and connection2Match


	fnb_miss = missDegree/mDegree if mDegree !=0 else 0
	fnb_over = overDegree/sDegree if sDegree !=0 else 0

	fnbc_miss = missConnection/mConnection if mConnection !=0 else 0
	fnbc_over = overConnection/sConnection if sConnection !=0 else 0

	fnb2c_miss = miss2Connection/m2Connection if m2Connection !=0 else 0
	fnb2c_over = over2Connection/s2Connection if s2Connection !=0 else 0

	wd = fnb_miss + fnb_over
	wc = (fnbc_miss + fnbc_over)/missDegree if missDegree != 0 else fnbc_miss + fnbc_over

	w2c = (fnb2c_miss + fnb2c_over)/missDegree if missDegree != 0 else fnb2c_miss + fnb2c_over
	w = 3 - wd - wc - w2c
	return (has_matched, w)


def get1neighbors(graph,node):
	return graph.neighbors(node)

def get2neighbors(graph, node):
	neighbors = set()
	for n in get1neighbors(graph,node):
		for n2 in get1neighbors(graph,n):
			if n2 != node:
				neighbors.add(n2)
	return neighbors

def nbConnection(graph, node):
	return graph.subgraph(graph.neighbors(node)).number_of_edges()

def nb2Connection(graph, node):
	#possible improvement : store info when computed in graph to reuse it
	two_neighbors= []
	for one_n in graph.neighbors(node):
		if one_n not in two_neighbors and one_n != node:
			two_neighbors.append(one_n)
		for two_n in graph.neighbors(one_n):
			if two_n not in two_neighbors and two_n != node:
				two_neighbors.append(two_n)
	return (graph.subgraph(two_neighbors).number_of_edges())

