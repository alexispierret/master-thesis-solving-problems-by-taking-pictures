import networkx as nx
from math import *
from collections import defaultdict
import pprint as pp

"""
This function creates a model for the sudoku problem.  By default, 
a 9 by 9 sudoku is created by it can be changed with the parameter -n
"""
def sudokuModel(n = 9):
	graph = nx.Graph() 
	nodes = ((i, j) for i in range(n+1) for j in range(n+1))
	graph.add_nodes_from(nodes)
	for x, y in graph.nodes_iter():
		neighbors = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)] #this is potential neighbours
		for px, py in neighbors:
			if graph.has_node((px, py)): #only real neighbours are added
	 			graph.add_edge((x,y), (px, py)) 
	return graph