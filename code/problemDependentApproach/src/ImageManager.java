import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

import static org.opencv.imgproc.Imgproc.*;

/**
 * Created by benjamin_de_wergifosse on 26/10/15.
 */
public class ImageManager {

    private static boolean debug = false;

    private static final int VERTICAL = 1;
    private static final int HORIZONTAL = 2;

    private static Comparator<Point> pointComparator = new Comparator<Point>() {
        @Override
        public int compare(Point o1, Point o2) {
            return (int)(Math.abs(o2.x) - Math.abs(o1.x));
        }
    };

    public static ImageViewer imageViewer = new ImageViewer();

    /**
     * This function loads the image in args[0] or a default one if empty
     * The image is resized to 1Mpx
     * The image is turned into CV_8UC1 (=grayscale)
     */
    public Mat[] loadImage(String[] args, boolean isDir) {
        int nbFiles;
        File directory;
        String[] files = null;

        if (args.length == 0)
            nbFiles = 1;
        else if (!isDir)
            nbFiles = args.length;
        else {
            directory = new File(args[0]);
            files = directory.list();
            files = Arrays.copyOfRange(files, 1, files.length);
            nbFiles = files.length;
        }

        Mat[] imgs = new Mat[nbFiles];

        for (int i = 0; i < nbFiles; i++) {
            if (args.length == 0)
                imgs[i] = Imgcodecs.imread("../images/simple2.jpg");
            else if (isDir) {
                String path = args[0] + files[i];
                System.out.println(path);
                imgs[i] = Imgcodecs.imread(path);
            } else
                imgs[i] = Imgcodecs.imread(args[i]);

            //Resizing and grayscale
            int[] newSize = getSize(imgs[i].cols(), imgs[i].rows(), 1000000.0);
            System.out.println(newSize[0] + " " + newSize[1]);
            Imgproc.resize(imgs[i], imgs[i], new Size(newSize[0], newSize[1]), 0, 0, INTER_AREA);
            //Imgproc.resize(imgs[i], imgs[i], new Size(newSize[0], newSize[1]));
            //cvtColor(imgs[i], imgs[i], COLOR_BGR2GRAY);
        }

        return imgs;
    }

    /**
     * Given the current height and the current width of the image,
     * this function returns the new height and the new width for a new image
     * respecting the given resolution and in the same ratio
     */
    private static int[] getSize(int height, int width, double resolution) {
        System.out.println(height + " " + width);
        double ratio = Math.sqrt(height * width / resolution);
        int[] newSize = {(int) (height / ratio), (int) (width / ratio)};
        return newSize;
    }

    /**
     * This function threshold the input image using an
     * adaptative thresholding technique
     */
    public void thresholding(Mat img) {
        //Imgproc.medianBlur(img, img, 5);
        Imgproc.GaussianBlur(img, img, new Size(5, 5), 0);
        imageViewer.write(img, "blurred");
        Imgproc.adaptiveThreshold(img, img, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 19, 5);
        //Imgproc.adaptiveThreshold(img, img, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 19, 10);

        //Imgproc.medianBlur(img, img, 7);
        //Imgproc.Canny(img, img, 50, 100); //max is 1020 :o
    }

    /**
     * This function returns a List<MatOfPoint> representing polygons founds in the image in argument
     * The size of the array is at most 5
     * The degree of the polygons are between 4 and 8 included
     * The polygons kept in the list are the 5 biggest one in terms of the area and
     * are ordered in increasing degree
     */
    public List<MatOfPoint> getPolygons(Mat img) {
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(img, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        //Imgproc.drawContours(original, contours, -1, new Scalar(255, 255,0), 3);
        //imageViewer.show(original);

        int n = 5;
        List<MatOfPoint> big_rectangles = new ArrayList<MatOfPoint>();

        for (int i = 0; i < contours.size(); i++) {

            MatOfPoint2f approximation = new MatOfPoint2f();
            Imgproc.approxPolyDP(new MatOfPoint2f(contours.get(i).toArray()), approximation, 20.0, true);

            //polygon need 4 sides
            if (approximation.rows() < 4)
                continue;
            if (approximation.rows() > 8)
                continue;
            big_rectangles.add(new MatOfPoint(approximation.toArray()));
        }

        Collections.sort(big_rectangles, new RectangleComparator("Area"));
        big_rectangles = big_rectangles.subList(0, Math.min(big_rectangles.size(), n));
        Collections.sort(big_rectangles, new RectangleComparator("nEdges"));

        return big_rectangles;
    }

    /**
     * This function returns a new thresholded Mat of size 500x500.
     * In input we have the whole thresholded Mat and
     * the contour of the polygon we want to remap.
     */
    public Mat remap(Mat threshold, MatOfPoint contour) {
        //If this is a polygon of higher degree, extract the good quad
        if (contour.rows() != 4)
            contour = extractRectangle(contour);

        //If a good quad hasn't been found, return null
        if (contour.rows() == 0)
            return null;

        Point[] orderedPoints = getOrderedPoints(contour);
        int resolution = 500;

        List<Point> source = new ArrayList<Point>(Arrays.asList(orderedPoints));
        Mat sourceMat = Converters.vector_Point2f_to_Mat(source);

        List<Point> dest = new ArrayList<>();
        dest.add(new Point(0, 0));
        dest.add(new Point(resolution, 0));
        dest.add(new Point(resolution, resolution));
        dest.add(new Point(0, resolution));
        Mat destMat = Converters.vector_Point2f_to_Mat(dest);

        Mat perspective = Imgproc.getPerspectiveTransform(sourceMat, destMat);

        Mat out = new Mat(resolution, resolution, threshold.type());
        Imgproc.warpPerspective(threshold, out, perspective, new Size(resolution, resolution));
        return out;
    }

    /**
     * Given a contour that is not a rectangle, this function tries to keep the four
     * best corners that corresponds to the contour of the sudoku.
     * I.e the 4 points equally spaced are kept
     */
    private MatOfPoint extractRectangle(MatOfPoint contour) {
        if (contour.rows() <= 4)
            return contour;

        List<MatOfPoint> possibleQuad = getSubsets(contour, 4);
        MatOfPoint bestQuad = new MatOfPoint();

        for (MatOfPoint currentQuad : possibleQuad){
            boolean isAcceptable = isAcceptableQuad(currentQuad, 20);
            if (isAcceptable && isBetterQuad(currentQuad, bestQuad))
                bestQuad = currentQuad;

        }
        return bestQuad;
    }

    /**
     * Given in input a MatOfPoint representing a contour of quadrilateral,
     * this function returns an array of Point that are ordered !
     * It is indeed needed to have the points ordered to remap the grid correctly
     */
    private Point[] getOrderedPoints(MatOfPoint points) {
        Point[] array = new Point[4];
        List<Integer> indexes = new ArrayList<Integer>();
        double min = Double.MAX_VALUE;
        int minIdx = -1;
        double max = 0;
        int maxIdx = -1;
        for (int i = 0; i < points.rows(); i++) {
            indexes.add(i);
            double x = points.get(i, 0)[0];
            double y = points.get(i, 0)[1];
            double dist = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
            if (dist > max) {
                max = dist;
                maxIdx = i;
            }
            if (dist < min) {
                min = dist;
                minIdx = i;
            }
        }
        array[0] = new Point(points.get(minIdx, 0)[0], points.get(minIdx, 0)[1]);
        array[2] = new Point(points.get(maxIdx, 0)[0], points.get(maxIdx, 0)[1]);

        indexes.remove(indexes.indexOf(minIdx));
        indexes.remove(indexes.indexOf(maxIdx));

        Point left1 = new Point(points.get(indexes.get(0), 0)[0], points.get(indexes.get(0), 0)[1]);
        Point left2 = new Point(points.get(indexes.get(1), 0)[0], points.get(indexes.get(1), 0)[1]);

        if (left1.x >= left2.x) {
            array[3] = left2;
            array[1] = left1;
        } else {
            array[3] = left1;
            array[1] = left2;
        }
        return array;
    }

    /**
     * Given two polygons in argument, this function returns true if the current quad is better
     * then the current best one.  A quad is better than another one if it's area is bigger.
     */
    private static boolean isBetterQuad(MatOfPoint current, MatOfPoint best){
        if (best.rows() != 4)
            return true;
        return Imgproc.contourArea(current) > Imgproc.contourArea(best);
    }

    /**
     * Given a quadrilateral and a tolerance in argument, this function determines if
     * the quad is acceptable or not.  A quad is considered acceptable if his 4 sides
     * are of the same length and each of his angle is a right angle.
     */
    private static boolean isAcceptableQuad(MatOfPoint quad, double tolPercentage){
        List<Point> points = quad.toList();

        double defaultDist = getMeanDistance(points);

        for(int i=0; i<4; i++){
            Point p1 = points.get(i);
            Point p2 = points.get((i+1)%4);
            Point p3 = points.get((i+2)%4);
            int dist12 = getDistance(p1, p2);
            int dist23 = getDistance(p2, p3);
            int dist31 = getDistance(p3, p1);
            //All sides of the square have the same length
            if (Math.abs(dist12 - defaultDist) > tolPercentage/100*defaultDist){
                return false;
            }
            //Each angle of the square is a right angle
            double angle = Math.acos( (Math.pow(dist31, 2)-Math.pow(dist12, 2)-Math.pow(dist23, 2))/(-2*dist12*dist23) );
            if ( (100-tolPercentage)/100*Math.PI/2 > angle || angle > (100+tolPercentage)/100*Math.PI/2 ){
                return false;
            }
        }
        return true;
    }

    /**
     * Given a quadrilateral in argument, this function computes the mean distance
     * of a side of the quad.
     */
    private static double getMeanDistance(List<Point> quad){
        double sum = 0;
        for(int i=0; i< quad.size(); i++)
            sum += getDistance(quad.get(i), quad.get((i+1)%4));
        return sum / 4.0;
    }

    /**
     * Given 2 points in argument, this function computes the distance in 2D between
     * these two points.
     */
    private static int getDistance(Point p, Point o){
        double pX = p.x;
        double pY = p.y;
        double oX = o.x;
        double oY = o.y;
        return (int) Math.sqrt(Math.pow(pX-oX, 2)+Math.pow(pY-oY, 2));
    }

    /**
     * This function is an helper for the function
     * getSubsets(MatOfPoint contour, int subsetSize)
     * See the doc of this function for more details.
     */
    private static void getSubsets(List<Point> contour, int subsetSize, int currentIndex,
                                   List<Point> currentList, List<MatOfPoint> sol){
        if (subsetSize == 0){
            MatOfPoint newSol = new MatOfPoint();
            newSol.fromList(currentList);
            sol.add(newSol);
            return;
        }
        for (int i=currentIndex; i<=contour.size()-subsetSize; i++){
            currentList.add(contour.get(i));
            getSubsets(contour, subsetSize-1, i+1, currentList, sol);
            currentList.remove(contour.get(i));
        }
    }

    /**
     * Given the contour of polygon, for example of degree 6, this function computes
     * all the possible subsets of points of subsetSize, for example 4.
     * This function is useful to generate all the possible quad from a
     * polygon of degree 5 or more and then keep the best one (i.e keep the sudodu)
     */
    private static List<MatOfPoint> getSubsets(MatOfPoint contour, int subsetSize){
        List<MatOfPoint> output = new ArrayList<MatOfPoint>();
        List<Point> currentList = new ArrayList<Point>();

        getSubsets(contour.toList(), subsetSize, 0, currentList, output);
        return output;
    }


    /**
     * From a grid in argument, this function will try to extract to extract
     * a sudoku with all his lines composing it.
     */
    public List<Point> getSudoku(Mat grid) throws IOException{
        List<Point> linesH = new ArrayList<>();
        List<Point> linesV = new ArrayList<>();
        List<Point> allLines = new ArrayList<>();
        getLines(grid, linesH, linesV);

        boolean sudokuFound =  isSudokuFound(linesV, linesH, 30);
        //System.out.println("Sudoku found is " + sudokuFound);

        //Check if the sudoku is found and extrapolate if needed
        if (sudokuFound){
            if (linesH.size()!=10)
                extrapolateLines(linesH);
            if (linesV.size()!=10)
                extrapolateLines(linesV);
            allLines.addAll(linesH);
            allLines.addAll(linesV);
            Mat getSudokuExtrapolated = grid.clone();
            drawLines(getSudokuExtrapolated, allLines, 3);
            imageViewer.write(getSudokuExtrapolated, "getSudokuExtrapolated");
            return allLines;
            //return new Sudoku(grid, linesH, linesV);
        }
        else if (debug){
            allLines.addAll(linesH);
            allLines.addAll(linesV);
            Mat temp = grid.clone();
            cvtColor(temp, temp, COLOR_GRAY2RGB);
            drawLines(temp, allLines, 2);
            imageViewer.show(temp, "debug");
        }
            return null;
    }

    /**
     * Applied to a Mat that is thresholded and remapped, this function populate
     * the two arrayList in argument with all the DISTINCT horizontal and vertical lines
     */
    private static void getLines(Mat grid, List<Point> linesH, List<Point> linesV) {
        Mat lines = new Mat();
        List<Point> allLines = new ArrayList<>();
        Mat negative= new Mat(grid.rows(),grid.cols(), grid.type(), new Scalar(255,255,255));
        Core.subtract(negative, grid, negative);

        //Imgproc.HoughLines(negative, lines, 1, Math.PI / 180, 200);
        Imgproc.HoughLines(negative, lines, 1, Math.PI/180, 300, 0, 0, 0.0, Math.PI*2);

        //Tolerance for the sin and cos to keep a vertical or horizontal line
        double tolTheta = 0.95;
        int tolRho = 25;
        double rho;
        double theta;

        //Keep only vertical and horizontal lines (WITH REDUNDANCY REMOVED)
        for (int i = 0; i < lines.rows(); i++) {
            rho = lines.get(i, 0)[0];
            theta = lines.get(i, 0)[1];
            Point line = new Point(rho, theta);
            allLines.add(line);
            if (Math.abs(Math.cos(theta)) > tolTheta)
                addLine(linesV, line, tolRho, VERTICAL);
            else if (Math.abs(Math.sin(theta)) > tolTheta){
                addLine(linesH, line, tolRho, HORIZONTAL);
            }
        }

        //Drawings
        Mat houghAll = grid.clone();
        drawLines(houghAll, allLines, 1);
        imageViewer.write(houghAll, "houghAll");

        Mat houghVandH = grid.clone();
        drawLines(houghVandH, linesH, 2);
        drawLines(houghVandH, linesV, 2);
        imageViewer.write(houghVandH, "houghVandH");

        //If too many lines found, keep the right one
        clearList(linesH, 1.2, 0.8);
        clearList(linesV, 1.2, 0.8);
        //System.out.println("hLines count " + linesH.size() + " vLines count " + linesV.size());


        //Drawings : suite
        Mat houghCleared = grid.clone();
        drawLines(houghCleared, linesH, 3);
        drawLines(houghCleared, linesV, 3);
        imageViewer.write(houghCleared, "houghCleared");

    }

    /**
     * Given an arrayList of lines of a certain orientation
     * and a potential line to add to this list, this line is added if it is distinct from the others
     * and replace its sibling it it is more horizontal or more vertical !
     * Else it is simply ignored.
     */
    private static boolean addLine(List<Point> linesList, Point line, int tol, int orientation){
        for (int i=0; i<linesList.size(); i++){
            //If there is a similar sibling...
            if(isLineSimilar(linesList.get(i), line, tol)){
                //... replace it if better
                if(orientation == VERTICAL && Math.abs(Math.cos(line.y)) > Math.abs(Math.cos(linesList.get(i).y))){
                    linesList.set(i, line);
                    return true;
                }

                else if(orientation == HORIZONTAL && Math.abs(Math.sin(line.y)) > Math.abs(Math.sin(linesList.get(i).y))){
                    linesList.set(i, line);
                    return true;
                }
                else
                    return false;
            }
        }
        //Add if distinct
        linesList.add(line);
        return true;
    }

    /**
     * This function compare two lines in argument according a tolerance (in pixels)
     * and return true if they are similar and false else.
     */
    private static boolean isLineSimilar(Point line1, Point line2, int tol){
        return Math.abs(Math.abs(line1.x) - Math.abs(line2.x)) < tol;
    }

    /**
     * When detecting a sudoku that is surrounded by a border, too many lines can be found
     * This function keep only the 10 lines that are equally spaced, i.e clear the list of lines
     */
    private static void clearList(List<Point> list, double upperBound, double lowerBound){
        //No need to clear if less than 10 lines
        if (list.size() <= 10)
            return;

        // System.out.println("Clearing the list with " + list.size() + " items");
        Collections.sort(list, pointComparator);

        int distSum = 0;
        int distMean = 0;
        int size = list.size();
        for (int i=0; i < list.size()-1; i++)
            distSum += Math.abs(Math.abs(list.get(i + 1).x) - Math.abs(list.get(i).x));
        distMean = distSum/(size-1);

        int currentSize = size;
        for (int i = size-1; i >=0 && currentSize>10; i--){
            if (i==currentSize-1){
                int distance = (int)(list.get(currentSize-1).x - list.get(currentSize-2).x);
                if (distance > distMean*upperBound || distance < distMean*lowerBound) {
                    list.remove(i);
                    currentSize--;
                }
            }
            else if (i==0){
                int distance = (int)(list.get(1).x - list.get(0).x);
                if (distance > distMean*upperBound || distance < distMean*lowerBound)
                    list.remove(i);
            }
            else {
                int distance1 = (int)(list.get(i+1).x - list.get(i).x);
                int distance2 = (int)(list.get(i).x - list.get(i-1).x);
                if ((distance1 > distMean*upperBound || distance1 < distMean*lowerBound) &&
                        (distance2 > distMean*upperBound || distance2 < distMean*lowerBound))
                    list.remove(i);
            }
        }
    }

    /**
     * When a sudoku is found (or rather : presumed to be found) but too few lines compose it
     * This function will extrapolate the missing lines at equal distance each from the other
     * in order to get a proper sudoku where we can extract cells for OCR
     */
    private static void extrapolateLines(List<Point> lines){
        System.out.println("Extrapolating the missing lines...");
        Collections.sort(lines, pointComparator);
        int size = lines.size();
        assert size > 0;
        double minX = Math.abs(lines.get(0).x);
        double minTheta = Math.abs(lines.get(0).y);
        minTheta = firstQuarterAngle(minTheta);
        double maxX = Math.abs(lines.get(size-1).x);
        double maxTheta = Math.abs(lines.get(size-1).y);
        maxTheta = firstQuarterAngle(maxTheta);
        double lineDist = (maxX-minX)/9;
        double meanTheta = (minTheta + maxTheta)/2.0;

        // System.out.println("minX is " + minX + " and maxX is " + maxX);
        // System.out.println("minTheta is " + minTheta + " and maxTheta is " + maxTheta);
        // System.out.println("lineDist is " + lineDist + " and meanTheta is " + meanTheta);

        int indexOk = 0;
        for(int i=0; i<size; i++){
            Point currentLine = lines.get(i);
            double currentLineX = Math.abs(currentLine.x);
            int indexCurrentLine = (int)Math.round((currentLineX-minX)/lineDist);
            if (indexCurrentLine > indexOk){
                //Add missing lines
                for(int j=indexOk; j<indexCurrentLine; j++)
                    lines.add(new Point(j*lineDist+minX, meanTheta));
                indexOk = indexCurrentLine;
            }
            indexOk++;
        }
    }

    /**
     * From any angle in argument, this function returns its equivalent in the first quarter
     * of the trigonometrical circle.
     */
    private static double firstQuarterAngle(double angle){
        while(!(angle >= 0 && angle <= Math.PI/2))
            angle = Math.abs(angle-Math.PI);
        return angle;
    }

    /**
     * This function tries to determine if a given list of horizontal and vertical lines
     * corresponds to a sudoku.
     * If the number of vertical and horizontal lines if greater than 5 we consider it is found
     * Else if the 4 MAIN HARD LINES are found equally spaced at the 4 third of the figure, it is
     * also considered to be found
     */
    private static boolean isSudokuFound(List<Point> linesV, List<Point> linesH, int tol){
        if (linesV.size() >= 5 && linesV.size() >= 5)
            return true;
        int nHardLinesFound = 0;
        for(Point line : linesH){
            int x = Math.abs((int)line.x);
            if ((0 <= x  && x <= tol) || (167-tol <= x && x <= 167+tol) ||
                    (333-tol <= x && x <= 333+tol ) || (500-tol <= x && x <=500))
                nHardLinesFound++;
        }
        for(Point line : linesV){
            int x = (int)line.x;
            if ((0 <= x  && x <= tol) || (167-tol <= x && x <= 167+tol) ||
                    (333-tol <= x && x <= 333+tol ) || (500-tol <= x && x <=500))
                nHardLinesFound++;
        }
        //System.out.println("nHardLines is " + nHardLinesFound);
        return (nHardLinesFound>=8);
    }

    /**
     * This function take a Mat in input and draw on it the line corresponding to the
     * theta and rho given in argument.
     * The line will be of the given size and in the given color
     */
    public static void drawLine(Mat drawOn, double rho, double theta, Scalar color, int size){
        Point pt1, pt2;
        double a = Math.cos(theta), b = Math.sin(theta);
        double x0 = a * rho, y0 = b * rho;
        pt1 = new Point(Math.round(x0 + 1000 * (-b)), Math.round(y0 + 1000 * (a)));
        pt2 = new Point(Math.round(x0 - 1000 * (-b)), Math.round(y0 - 1000 * (a)));
        Imgproc.line(drawOn, pt1, pt2, color, size);
    }

    public static void drawLines(Mat drawOn, List<Point> lines, int size){
        if (drawOn.type() == 0)
            cvtColor(drawOn, drawOn, COLOR_GRAY2RGB);
        for(Point l : lines){
            drawLine(drawOn, l.x, l.y, new Scalar(51, 173, 255), size);
        }
    }

}

/*
* Comparator for rectangles
*/
class RectangleComparator implements Comparator<MatOfPoint> {
    private String order;
    public RectangleComparator(String order){
        this.order = order;
    }
    @Override
    public int compare(MatOfPoint a, MatOfPoint b) {
        if (order.equals("Area"))
            return (int) (Imgproc.contourArea(b) - Imgproc.contourArea(a));
        else if (order.equals("nEdges"))
            return a.rows() - b.rows();
        else
            return -1;
    }
}






