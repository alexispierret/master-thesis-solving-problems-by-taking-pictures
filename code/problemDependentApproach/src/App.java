import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.opencv.imgproc.Imgproc.*;

/**
 * App Welcome
 * Benjamin de Wergifosse & Alexis Pierret
 * ../images/curved1.jpg ../images/dark3.jpg ../images/darkCurved1.jpg ../images/simple1.jpg ../images/simple6.jpg ../images/simple8.jpg
 */

public class App {
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void println(String str){
        System.out.println(str);
    }

    public static void main(String [] args) throws IOException {
        Random r = new Random();

        System.out.println("Welcome in Sudoku Solver Image Recognition :D");
        //Init
        ImageManager imageManager = new ImageManager();
        ImageViewer imageViewer = new ImageViewer();

        //TODO manage the image not loaded correctly...

        boolean folder = false;
        if (args[0].charAt(args[0].length()-1) == '/')
            folder = true;
        Mat[] imgs = imageManager.loadImage(args, folder); //resized

        //Looping over all the images
        for (int i=0; i < imgs.length; i++) {
            Mat img = imgs[i];
            Mat original = imgs[i].clone();

            //imageViewer.show(original, "Original picture");
            imageViewer.write(original, "original");

            cvtColor(img, img, COLOR_BGR2GRAY);
            //imageViewer.show(img, "Grayscaled image");
            imageViewer.write(img, "grayscale");

            //THRESHOLDING the image
            imageManager.thresholding(img);
            //imageViewer.show(img, "Thresholded Image");
            imageViewer.write(img, "thresholded");

            //GET POLYGONS
            List<MatOfPoint> polygons = imageManager.getPolygons(img.clone());
            int size = polygons.size();
            for (int j = 0; j < size; j++) {
                //polygons.set(j, imageManager.extractRectangle(polygons.get(j)));
                Scalar color = new Scalar(r.nextInt(255), r.nextInt(255), r.nextInt(255));
                Imgproc.drawContours(original, polygons, j, color, 2 * (size - j));
            }

            println("Displaying the polygons found ...");
            imageViewer.write(original, "polygons");
            imageViewer.show(original, "polygons found");

            //REMAP the grids
            for (MatOfPoint polygon : polygons){
                Mat remap = imageManager.remap(img, polygon);

                if (remap != null) {
                    //GET LINES OF THE GRID
                    List<Point> lines = imageManager.getSudoku(remap);
                    if (lines != null) {
                        imageViewer.write(remap, "remappingRemapped");
                        cvtColor(remap, remap, COLOR_GRAY2RGB); //In order to draw red lines on it ^^
                        System.out.println(lines.size() + " lines are going to be drawn");
                        for(Point item : lines){
                            //Scalar color = new Scalar(r.nextInt(255), r.nextInt(255), r.nextInt(255));
                            imageManager.drawLine(remap, item.x, item.y, new Scalar(0, 0, 255), 2);
                        }
                        imageViewer.show(remap, "lines on the sudoku");
                    }
                }
            }
                
        }
    }
}
