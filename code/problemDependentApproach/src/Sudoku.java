import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import net.sourceforge.tess4j.Tesseract1;
import net.sourceforge.tess4j.TesseractException;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

/**
 * Created by benjamin_de_wergifosse on 26/10/15.
 */
public class Sudoku {
	List<Point> linesH;
	List<Point> linesV;
	Mat img;
	Mat[][] cells;
	public String[][] cellsNum;

	/*
	* Constructor of a sudoku object
	* linesH and linesV respectively correspond to horizontal and vertical lines
	*/
    public Sudoku(Mat img, List<Point> linesH, List<Point> linesV) throws IOException{
    	this.linesH = linesH;
    	this.linesV = linesV;
    	this.img = img;
    	List<Point> inter = findInter();
    	cells = new Mat[9][9] ;
    	cellsNum = new String[9][9] ;
    	setCells(img, inter);
    }
    
    /*
	* Performs the OCR on the subc BufferedImage
    */
    public String getNum(BufferedImage subc, Tesseract1 instance){
        String name=null;

        try {           
            name=instance.doOCR(subc);
        } catch (TesseractException e) {System.err.println(e.getMessage());}
        
        name=new StringTokenizer(name,"\n").nextToken();
        return name;
    }
    /*
	* Return a BufferedImage image from its Mat matrix representation
	*/
    public static BufferedImage mat2Img(Mat in) throws IOException {
    	MatOfByte bytemat = new MatOfByte();
    	Imgcodecs.imencode(".jpg", in, bytemat);
    	byte[] bytes = bytemat.toArray();
    	InputStream stream = new ByteArrayInputStream(bytes);
    	BufferedImage img = ImageIO.read(stream);
        return img;
    } 
    
    /*
	* From the image and the sudoku points retrieved, split and perform
	* OCR on each cell of the expected sudoku
	*/
    
    public void setCells(Mat input, List<Point> points) throws IOException{
    	int border = 0;
    	Tesseract1 instance = new Tesseract1();

        instance.setLanguage("eng");
        instance.setTessVariable("tessedit_char_whitelist", "123456789");

        for(int j = 0; j<9; j++){
        	for(int i = 0; i<9; i++){
        		//identify the cell
				Point leftUp = points.get(i*10+j);
				Point rightBot = points.get(i*10+j+11);
				Rect roi = new Rect( (int)Math.floor(leftUp.x)+border,  (int)Math.floor(leftUp.y)+border, (int)Math.floor(rightBot.x-leftUp.x)-2*border, (int)Math.floor(rightBot.y-leftUp.y)-2*border);
				cells[i][j]  = new Mat(input, roi);
				List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
				
				Imgproc.findContours(cells[i][j].clone(),  contours, new Mat(), Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

				int add = 2;
				Rect bestRectangle = new Rect();
				boolean digit = false;

				//check if exist contours large enough to be a digit
				for (int k = 0; k < contours.size(); k++) {
					MatOfPoint2f approximation = new MatOfPoint2f();
		            Imgproc.approxPolyDP(new MatOfPoint2f(contours.get(k).toArray()), approximation, 3.0, true);
		            Rect rect = Imgproc.boundingRect(new MatOfPoint(approximation.toArray()));
		            rect = new Rect(rect.x-add, rect.y-add, rect.width+2*add, rect.height+2*add);
		            int centerX = rect.x + rect.width/2;
		            int centerY = rect.y + rect.height/2;
		            if(rect.area()<2000 && rect.area()>250 && bestRectangle.area()<rect.area() && 
		            		 centerX>20 && centerX < 35 && centerY>20 && centerY < 35){
		            	bestRectangle = rect;
		            	digit = true;
		            }
		            	
				}  
				
	            cellsNum[i][j] = ".";
            	if(digit){
            		Rect rect = bestRectangle;
            		cells[i][j] = new Mat(cells[i][j], bestRectangle);
        			BufferedImage aa = mat2Img((cells[i][j]));
            		String result = getNum(aa, instance);
            		cellsNum[i][j] = result;
            	}
			}
		}

	}

	/*
	* Find from the vertical and horizontal lines the
	* ordered set of intersections of the sudoku grid
	*/
    public List<Point> findInter(){
		List<Point> list = new ArrayList<Point>();
		for(int i = 0; i<linesV.size(); i++){
			Point vert1 = linesV.get(i);
			double rho1 = vert1.x;
			double theta1 = vert1.y;
			//System.out.println(i);
			for(int j = 0; j<linesH.size(); j++){
				//System.out.println(j);
				Point hori1 = linesH.get(j);
				double rho2= hori1.x;
				double theta2 = hori1.y;
				double c = 1/(Math.cos(theta1)*Math.sin(theta2)-Math.sin(theta1)*Math.cos(theta2));
				double x = c*(Math.sin(theta2)*rho1-Math.sin(theta1)*rho2); 
				double y = c*(Math.cos(theta1)*rho2-Math.cos(theta2)*rho1);
				if(x >=0 && y >=0)
					list.add(new Point(x,y));
			}
		}
		Collections.sort(list, new PointComparator());
		return list;
	}
}

// comparator for the 2D points
class PointComparator implements Comparator<Point> {
    @Override
    public int compare(Point a, Point b) {
    	if(Math.abs((a.x-b.x)/a.x)<0.05){//if same X component
    		return (int) (a.y-b.y);
    	}
    	else
    		return (int) (a.x-b.x);
    }
}