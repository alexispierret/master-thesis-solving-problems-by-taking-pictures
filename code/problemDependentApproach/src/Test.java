import org.opencv.core.*;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.Objdetect;

import java.beans.FeatureDescriptor;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.opencv.imgproc.Imgproc.*;

class Test {
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main (String [] args){

        ImageViewer iv = new ImageViewer();

        Mat img_object = Imgcodecs.imread("../images/default.jpg", COLOR_BGR2GRAY);
        Mat img_scene = Imgcodecs.imread("../images/default1.jpg", COLOR_BGR2GRAY);

        FeatureDetector detector = FeatureDetector.create(FeatureDetector.SURF);
        DescriptorExtractor extractor = DescriptorExtractor.create(DescriptorExtractor.SURF);
        DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE);

        MatOfKeyPoint keypoints_object = null, keypoints_scene = null;
        detector.detect(img_object, keypoints_object);
        detector.detect(img_scene, keypoints_scene);

        Mat descriptors_object = null, descriptors_scene = null;
        extractor.compute( img_object, keypoints_object, descriptors_object);
        extractor.compute( img_scene, keypoints_scene, descriptors_scene);


        MatOfDMatch matchs = new MatOfDMatch();
        matcher.match(descriptors_object, descriptors_scene, matchs);

        int N = 50;
        DMatch[] tmp03 = matchs.toArray();
        DMatch[] tmp04 = new DMatch[N];
        for (int i=0; i<tmp04.length; i++) {
            tmp04[i] = tmp03[i];
        }
        matchs.fromArray(tmp04);

        Mat matchedImage = new Mat(img_object.rows(), img_object.cols()*2, img_object.type());
        Features2d.drawMatches(img_object, keypoints_object, img_scene, keypoints_scene, matchs, matchedImage);

        iv.show(matchedImage);
    }
}
