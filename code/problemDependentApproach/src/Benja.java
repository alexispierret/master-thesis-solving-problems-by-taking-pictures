import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.opencv.imgproc.Imgproc.*;

class Benja {
	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}
	
	public static Mat loadImage(String filePath){
		return Imgcodecs.imread(filePath);
	}
	
	public static void displayImage(Mat img){
		ImageViewer imageViewer = new ImageViewer();
	    imageViewer.show(img, "Loaded image");
	}

    public static void training(Mat img){
        Random r = new Random();

        int v = 9;
        //MEAN BLUR
        Mat meanBlur = new Mat();
        Imgproc.blur(img, meanBlur, new Size(v,v));

        //GAUSSIAN BLUR
        Mat gaussianBlur = new Mat();
        Imgproc.GaussianBlur(img, gaussianBlur, new Size(v,v), 0);

        //MEDIAN BLUR (used for salt and pepper noise reduction)
        Mat medianBlur = new Mat();
        Imgproc.medianBlur(img, medianBlur, v);

        //GRAYSCALE img
        Mat grayScale = new Mat();
        cvtColor(img, grayScale, COLOR_BGR2GRAY);

        //BINARY THRESHOLD for grayscale image
        //Observations : between 50 and 100 is the best
        Mat binaryThresh1 = new Mat();
        Mat binaryThresh2 = new Mat();
        Mat binaryThresh3 = new Mat();
        Imgproc.threshold(grayScale, binaryThresh1, 50, 255, Imgproc.THRESH_BINARY);
        Imgproc.threshold(grayScale, binaryThresh2, 75, 255, Imgproc.THRESH_BINARY);
        Imgproc.threshold(grayScale, binaryThresh3, 100, 255, Imgproc.THRESH_BINARY);

        //ADAPTATIVE THRESHOLING (noise reduction can be applied BEFORE adaptative thresholding)
        Mat adaptiveThresh = new Mat();
        Imgproc.adaptiveThreshold(grayScale, adaptiveThresh, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 101, 10);

        //EDGE DETECTION (applied to a grayscale image)
        Mat edgeDetection = new Mat();
        Mat blur1 = new Mat();
        Mat blur2 = new Mat();
        Imgproc.GaussianBlur(grayScale, blur1, new Size(9, 9), 0);
        Imgproc.GaussianBlur(grayScale, blur2, new Size(15, 15), 0);
        Core.absdiff(blur1, blur2, edgeDetection);
        //displayImage(edgeDetection);
        Core.multiply(edgeDetection, new Scalar(100), edgeDetection);
        //displayImage(edgeDetection);
        Imgproc.threshold(edgeDetection, edgeDetection, 50, 255, THRESH_BINARY_INV);

        //CANNY EDGE DETECTION (widely used) (applied to grayscale image)
        Mat cannyDetection = new Mat();
        System.out.println(img);
        System.out.println(grayScale);
        MatOfDouble mean = new MatOfDouble();
        MatOfDouble stddev = new MatOfDouble();
        Core.meanStdDev(grayScale, mean, stddev);
        double meanVal = mean.get(0,0)[0]/255*1020;
        double stddevVal = stddev.get(0,0)[0]/255*1020;
        System.out.println(meanVal + " " + stddevVal);
        //Imgproc.Canny(grayScale, cannyDetection, meanVal-0.5*stddevVal, meanVal+0.5*stddevVal); //max is 1020 :o
        Imgproc.Canny(grayScale, cannyDetection, 100, 200); //max is 1020 :o
        displayImage(cannyDetection);


       //HARRIS CORNER DETECTION (grayscale image)
        Mat harrisDetection = new Mat();
        Mat tempDst = new Mat();
        Imgproc.cornerHarris(grayScale, tempDst, 2, 3, 0.04);
        Mat tempsDstNorm = new Mat();
        Core.normalize(tempDst, tempsDstNorm, 0, 255, Core.NORM_MINMAX);
        Core.convertScaleAbs(tempsDstNorm, harrisDetection);

        for(int i = 0; i < tempsDstNorm.cols(); i++)
            for(int j = 0; j < tempsDstNorm.rows(); j++)
                if (tempsDstNorm.get(j,i)[0] > 150)
                    Imgproc.circle(harrisDetection, new Point(i,j), 5, new Scalar(r.nextInt(255)), 2);

        //HOUGH TRANSFORM
        Mat lines = new Mat();
        Mat houghTransform = img.clone();
        Imgproc.HoughLinesP(cannyDetection, lines, 1, Math.PI/180, 50, 20, 20);
        for (int i = 0; i < lines.cols(); i++){
            System.out.println("new line");
            double[] points = lines.get(0,i);
            double x1, x2, y1, y2;
            x1 = points[0];
            y1 = points[1];
            x2 = points[2];
            y2 = points[3];

            Point pt1 = new Point(x1, y1);
            Point pt2 = new Point(x2, y2);

            Imgproc.line(houghTransform, pt1, pt2, new Scalar(255, 0, 0), 3);
        }

/*
        //CONTOURS FINDING
        Mat hierarchy = new Mat();
        Mat contoursFinding = img.clone();
        List<MatOfPoint> contourList = new ArrayList<MatOfPoint>();
        Imgproc.findContours(cannyDetection, contourList, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        MatOfPoint biggestContour = contourList.get(0);
        int biggestIndex = 0;
        for(int i = 0; i < contourList.size(); i++){
            Imgproc.drawContours(contoursFinding, contourList, i, new Scalar(r.nextInt(255), r.nextInt(255), r.nextInt(255)), 3);
            if (contourArea(contourList.get(i)) > contourArea(biggestContour)){
                biggestContour = contourList.get(i);
                biggestIndex = i;
            }
        }
        Imgproc.drawContours(contoursFinding, contourList, biggestIndex, new Scalar(r.nextInt(255), r.nextInt(255), r.nextInt(255)), -1);
*/
        //DISPLAY IMAGES
        displayImage(img);
        displayImage(meanBlur);
        displayImage(gaussianBlur);
        displayImage(medianBlur);
        displayImage(grayScale);
        displayImage(binaryThresh1);
        displayImage(binaryThresh2);
        displayImage(binaryThresh3);
        displayImage(adaptiveThresh);
        //displayImage(edgeDetection);
        //displayImage(cannyDetection);
        //displayImage(harrisDetection);
        //displayImage(houghTransform);
        //displayImage(contoursFinding);
    }

	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		
		//String path = "../images/dark3.jpg";
        //String path = "../images/default1.jpg";
        String path = "../images/simple2bis.jpg";
		Mat img = loadImage(path);

		if(img.dataAddr()==0)
			System.out.println("Couldn't open file " + path);
		else{
            training(img);
		}
		
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("elapsedTime : " + elapsedTime + "ms");
	}
}