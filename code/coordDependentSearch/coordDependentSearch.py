import sys
import cv2
import math

import preprocessing
import graphDetection
import utils

import matplotlib.pyplot as plt

import models
import graphMatching

import ocr

import pprint as pp

# ======PARAMETERS DEFINITION======
resize_size = 1000000
show, saveResult, verbose, solution = True, False, False, False

allImg = [	'curved1', 'curved2', 'curved3', 'curved4', 'curved0', 'curved0Noisy'
			'dark1', 'dark2', 
			'default1', 'default2',
			'far1', 
			'orientation1', 'orientation2', 
			'reversed1', 
			'simple1', 'simple2', 'simple3', 'simple4', 'simple5', 'simple6', 'simple7', 'simple8', 
			'sudoku2', 'sudoku3', 'sudoku4', 'sudoku6']
simpleImg = ['simple1', 'simple2', 'simple3', 'simple4', 'simple5', 'simple6', 'simple7', 'simple8']
darkImg = [ 'dark1', 'dark2']
curvedImg = ['curved1', 'curved2', 'curved3', 'curved4']
persoImg = ['orientation1']
defaultSudoku = ['simple1']
defaultPyramidMath = ['pyramidBasic1']
# ======END OF PARAMETERS DEFINITION======


"""
This function contains the whole logic of the coordinates 
dependant search.  It takes a -problemName in argument, for 
example 'sudoku' or 'mathPyramid' and a list of images on which
to perform the search.  The main steps of the algorithm are 
	STEP 1 - Preprocessing image
	STEP 2 - Graph detection
	STEP 3 - Coordinates Dependant Search
	STEP 4 - Display matching
	STEP 5 - OCR
	STEP 6 - Resolution of the problem
"""
def coordDependantSearch(problemName, images) : 
	plt.ion()
	for imgName in images : 
		print "===> Image", imgName
		imgPath = "../../images/" + imgName
		if imgPath[-4:] != '.jpg': 
			imgPath += ".jpg"
		
		#Loading image
		img = cv2.imread(imgPath)
		if img is None : 
			print "Wrong image name"
			sys.exit()

		#STEP 1 - Preprocessing image
		print "\t Preprocessing the image"
		resized			=	preprocessing.resize(img, resize_size)
		biFiltered 		=	preprocessing.bilateral_filter(resized, -1,  12, 5)
		thresh 			= 	preprocessing.adaptive_threshold_gaussian(resized, 19, 10)
		eroded 			= 	preprocessing.dilateErode(thresh)

		#STEP 2 - Graph detection
		print "\t Graph detection inside of the image"
		thin 			= 	graphDetection.guo_hall_thinning(eroded)
		sGraphNoFil		=	graphDetection.node_detection(thin)
		graphDetection.edge_detection(thin, sGraphNoFil)
		sGraph 			=	sGraphNoFil.copy()
		graphDetection.merge_close_node_filter(sGraph, 5)
		graphDetection.connected_components_filter(sGraph, 8)

		#STEP 3 - Coordinates Dependant Search
		print "\t Starting the coordinates dependant search"
		mGraph 			= 	models.getModel(problemName)
		matching, eX 	=	graphMatching.search(mGraph, sGraph, verbose)

		#STEP 4 - Display matching
		print "\t Display the best matching found"
		imgSGraphNoFil	= 	utils.draw_graph(biFiltered.copy(), sGraphNoFil)
		imgSGraph 		= 	utils.draw_graph(biFiltered.copy(), sGraph)
		utils.showMatching(problemName, mGraph, matching, imgSGraph, imgName, eX)

		#STEP 5 - OCR
		print "\t Optical character recognition"
		cells 			=	ocr.extractCells(problemName, matching, thresh)
		cellsDecrypted 	= 	ocr.ocr(cells, False)
		pp.pprint(cellsDecrypted)

		#STEP 6 - Resolution of the problem
		if solution : 
			print '\t Resolution of the problem'
			sol 			=	models.sudokuResolution(cellsDecrypted)
			pp.pprint(map(list, zip(*sol)))


		#In boolean are true, display and/or save intermediate images
		if saveResult:
			utils.saveResult(resized, biFiltered, thresh, eroded, thin, imgSGraphNoFil, imgSGraph, imgName)
		if show:
			utils.showImages(imgSGraph, biFiltered, thresh, eroded, thin, imgSGraphNoFil, imgSGraph, imgName)
	plt.ioff()
	plt.show()



if __name__ == "__main__":
	usageMessage = (" ==============\n" +
				"| Usage notice |\n" +
				" ==============\n" +
				"  arg-1 : \t\tproblem type (sudoku or pyramidMath)\n" +
				"  arg-2 : \t\tmust be the value noSolution or solution to allow\n" +
				"\t\t\the resolution of the problem or not.  The resolution is only\n" + 
				"\t\t\tavailable for the sudoku problem\n" +
				"  [arg-3 -> arg-n] :\ta list of images names where to perform the search\n" + 
				"\t\t\tif not supplied, a default image is used\n" +
				"\t\t\tSpecial values can be used for sudokus: \n" + 
				"\tallImg\t\tIt allows to launch the algorithm over all images\n" + 
				'\tcurvedImg\tIt allows to launch the algorithm over the curved images\n' + 
				'\tsimpleImg\tIt allows to launch the algorithm over the simple images\n\n\n' +
				'Usage examples : \n' + 
				'\tpython coordDependantSearch.py sudoku noSolution allImg\n' +
				'\tpython coordDependantSearch.py sudoku Solution simpleImg\n' +
				'\tpython coordDependantSearch.py sudoku noSolution orientation1 orientation2\n' +
				'\tpython coordDependantSearch.py pyramidMath noSolution')

	n = len(sys.argv)
	if n == 1 or n == 2: 
		print "Number of args mismatched\n"
		print usageMessage
	elif n == 3 : 
		problemType = sys.argv[1]
		sol = sys.argv[2]
		if sol == 'solution' : 
			solution = True
		elif sol == 'noSolution' : 
			solution = False
		else : 
			print 'Unknown second argument\n'
			print usageMessage
			sys.exit(-1)
		if problemType == 'sudoku' : 
			coordDependantSearch(problemType, simpleImg)
		elif problemType == 'pyramidMath' : 
			coordDependantSearch(problemType, defaultPyramidMath)
		else : 
			print 'Unknown problemType defined\n'
			print usageMessage
			sys.exit(-1)

	else : 
		problemType = sys.argv[1]
		sol = sys.argv[2]
		args = sys.argv[3:]
		if sol == 'solution' : 
			solution = True
		elif sol == 'noSolution' : 
			solution = False
		else : 
			print 'Unknown second argument\n'
			print usageMessage
			sys.exit(-1)
		if args[0] == 'allImg' : 
			args = allImg
		elif args[0] == 'curvedImg' : 
			args = curvedImg
		elif args[0] == 'simpleImg' : 
			args = simpleImg

		if problemType == 'sudoku' : 
			coordDependantSearch(problemType, args)
		elif problemType == 'pyramidMath' : 
			coordDependantSearch(problemType, args)
		else : 
			print 'Unknown problemType defined\n'
			print usageMessage
			sys.exit(-1)