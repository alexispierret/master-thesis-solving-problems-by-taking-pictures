import utils
import numpy as np
import cv2
import sys
from random import randint


"""
This function extracts the well detected cells and 
proceed to a perspective remapping to be a perfect square for the sudoku problem
	-matchingList is the best match found
	-img is a thresholed image where to extract the cells
"""
def extractSudokuCells(matchingList, img, sudokuSize=9) : 
	matching = {key:value[0] for key,value in matchingList.iteritems() if value != []}
	sudokuCells = {}

	for i in range(1, sudokuSize+1) :
		for j in range(1, sudokuSize+1) :
			try : 
				topLeft			= list(matching[(i-1, j-1)])
				topRight		= list(matching[(i, j-1)]) #toChange if convention (x,y) or matrix (i,j)
				bottomLeft		= list(matching[(i-1, j)]) #toChange if convention (x,y) or matrix (i,j)
				bottomRight		= list(matching[(i, j)])
				corners = [topLeft, topRight, bottomRight, bottomLeft]

				resolution 		= int(utils.dist(topLeft, topRight))

				xMin = min(corners, key = lambda x : x[0])[0]
				xMax = max(corners, key = lambda x : x[0])[0]
				yMin = min(corners, key = lambda x : x[1])[1]
				yMax = max(corners, key = lambda x : x[1])[1]

				sourceCoord = np.array(corners, dtype = "float32")
				destCoord 	= np.array([[0, 0], [resolution, 0], [resolution, resolution], [0, resolution]], dtype = "float32")

				perspective = cv2.getPerspectiveTransform(sourceCoord, destCoord)
				sudokuCells[(i,j)] = cv2.warpPerspective(img, perspective, (resolution, resolution))
			
			except KeyError : 
				pass
	return sudokuCells

"""
This function extracts the well detected cells and 
proceed to a perspective remapping to be a perfect square for the pyramidMath problem
	-matchingList is the best match found
	-img is a thresholed image where to extract the cells
"""
def extractPyramidMathCells(matchingList, img, n=9) :
	matching = {key:value[0] for key,value in matchingList.iteritems() if value != []}
	pyramidCells = {}
	
	for y in range(n) : 
		for x in range(n-y) : 
			try :
				currentCell 	= (x,y)
				bottomLeftBase 	= (y+2*x, 2*y)
				bottomLeft 		= list(matching[bottomLeftBase])
				bottomRight 	= list(matching[(bottomLeftBase[0]+2, bottomLeftBase[1])])
				topLeft 		= list(matching[(bottomLeftBase[0], bottomLeftBase[1]+2)])
				topRight 		= list(matching[(bottomLeftBase[0]+2, bottomLeftBase[1]+2)])

				corners 		= [topLeft, topRight, bottomRight, bottomLeft]

				resolution 		= int(utils.dist(topLeft, topRight))
				xMin 			= min(corners, key = lambda x : x[0])[0]
				xMax 			= max(corners, key = lambda x : x[0])[0]
				yMin 			= min(corners, key = lambda x : x[1])[1]
				yMax 			= max(corners, key = lambda x : x[1])[1]

				#cv2.imshow('Before ' + str(currentCell), img[yMin:yMax, xMin:xMax])
				
				sourceCoord = np.array(corners, dtype = "float32")
				destCoord 	= np.array([[0, 0], [resolution, 0], [resolution, resolution], [0, resolution]], dtype = "float32")

				perspective = cv2.getPerspectiveTransform(sourceCoord, destCoord)
				pyramidCells[currentCell] = cv2.warpPerspective(img, perspective, (resolution, resolution))

				#cv2.imshow('After ' + str(currentCell), pyramidCells[currentCell])
				cv2.waitKey()
			except KeyError : 
				pass
	return pyramidCells


"""
This function is a helper calling the corresponding function to 
extract the celles according the -problemType in argument
"""
def extractCells(problemType, matching, thresh, size=9) : 
	if problemType == 'sudoku' : 
		return extractSudokuCells(matching, thresh, size)
	elif problemType == 'pyramidMath' : 
		return extractPyramidMathCells(matching, thresh, size)



"""
This function performs an OCR on the -cellsMapping given in argument.
It can also be used to perform the training on the instances.
A K-nearest neighbours scheme is used.
"""
def ocr(cellsMapping, training=False) : 
	matchingDecrypted = {}

	samples = []
	answers = []
	keys = {38:1, -55:2, 34:3, 39:4, 40:5, -89:6, -56:7, 33:8, -57:9, -64:0}

	#Trained on simples images and default
	if not training : 
		trainingData = np.loadtxt('training.data',np.float32)
		labelsData = np.loadtxt('labels.data',np.float32)
		labelsData = labelsData.reshape((labelsData.size,1))

		model = cv2.ml.KNearest_create()
		model.train(trainingData, cv2.ml.ROW_SAMPLE, labelsData) 

	for key, val in cellsMapping.iteritems() : 
		#(height, width) = val.shape
		#_, val = cv2.threshold (val, 70, 255, cv2.THRESH_BINARY)
		val = val[1:-1, 1:-1]
		val = cv2.resize(val, (30,30))

		_, contours, _ = cv2.findContours(val.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
		backtorgb = cv2.cvtColor(val,cv2.COLOR_GRAY2RGB)

		[xC,yC,wC,hC] = [0,0,0,0]
		for cnt in contours:
			[x,y,w,h] = cv2.boundingRect(cnt)
			#print x, y, h, w, cv2.arcLength(cnt, False)
			if 12 <= h <= 24 and 5 <= w <= 22 and cv2.arcLength(cnt,False) > 28 and x < 18 and y < 18 and (x > 5 or y > 5) : #digit found
				if h*w > wC*hC : 
					[xC,yC,wC,hC] = [x,y,w,h]

		if [xC,yC,wC,hC] != [0,0,0,0] : #digit found
			numberCell = val[yC:yC+hC, xC:xC+wC]
			resized = cv2.resize(numberCell,(20,20))	
			if training : 
				cv2.imshow(str(key), resized)
				keyPressed = cv2.waitKey(0)

				if keyPressed == 27 : 
					sys.exit()
				elif keyPressed in keys : 
					print 'training detection', keys[keyPressed], 'for', key
					answers.append(keys[keyPressed])
					sample = resized.reshape((1, 400))
					samples.append(sample[0])
				else : 
					continue
			else :
				resizedBis = resized.reshape((1,400))
				resizedBis = resizedBis.astype(np.float32)
				ret, res, neigh, dist = model.findNearest(resizedBis, 1)
				number = int((res[0][0]))
				matchingDecrypted[key] = number
				#cv2.imshow(str(key) + ' is ' + str(number) + '  h, w ' + str(h) + ',' + str(w), resized)
				#cv2.waitKey()
		
	if training : 
		print "training complete - saving results"
		tFile = open('training.data', 'a')
		lFile = open('labels.data', 'a')
		answers = np.array(answers,np.float32)
		
		np.savetxt(tFile,samples)
		np.savetxt(lFile,answers)

	return matchingDecrypted