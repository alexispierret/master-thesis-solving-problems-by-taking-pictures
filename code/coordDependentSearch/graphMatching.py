import networkx as nx
from math import *
from collections import defaultdict, deque
import models
import time as t
import operator
import utils
from sets import Set


"""
This is the main search function.
Given two graph structures -mGraph and -sGraph, the function
tries to find the best matching between the two graphs.  -mGraph is
the model and -sGraph is the search space graph.
"""
def search(mGraph, sGraph, verbose=False, shuffle=False):

	nEdges = mGraph.number_of_edges()/2
	#Output variables
	bestSedgesExplo = Set()
	bestMatching = defaultdict()
	bestMatchingValue = 0

	searchNumber = 0

	mFeatures = models.getFeatures(mGraph)
	startingNodes = getBasicMatchings(mFeatures, sGraph, shuffleMode=shuffle)

	for sN in startingNodes :
		searchNumber += 1
	  	startM, startS, startTransfo, stats = sN
		if verbose : print 'basicMatching', startM, startS, startTransfo, stats
		
		it = 0
		
		queue = deque([(startM, startS, startTransfo, startTransfo, 0)])
		matching = defaultdict(lambda : defaultdict(list))
		matching[startM][startS].append((startTransfo,0, 0, 0))


		exploredMEdge = Set()
		exploredSEdge = Set()
		mEdgesMatched = Set()

		#Keep exploring while new nodes are found
		while queue :
			it+=1
			(mNode, sNode, transfo, previousTransfo, nextra) = queue.pop()

			#Explore the neighbourhood of the current mNode
			genM = ((mNeighbour, mAttr) for (_, mNeighbour, mAttr) in mGraph.out_edges(mNode, data=True) if (min(mNode, mNeighbour), max(mNode, mNeighbour)) not in exploredMEdge)
			for mNeighbour, mAttr in genM :	
				mEdge = (min(mNode, mNeighbour), max(mNode, mNeighbour))
				exploredMEdge.add(mEdge)
				tMAttr = transformAttributes(mAttr, transfo)
				ptMAttr = transformAttributes(mAttr, previousTransfo)

				#And explore the neighbourhood of the current sNode to find correspondences to mNeighbour
				genS = ((sNeighbour, sAttr) for (_, sNeighbour, sAttr) in sGraph.out_edges(sNode, data=True) if (min(sNode, sNeighbour), max(sNode, sNeighbour)) not in exploredSEdge)
				for sNeighbour, sAttr in genS :
					sEdge = (min(sNode, sNeighbour), max(sNode, sNeighbour))
					matchSuccess, error = match(tMAttr, sAttr)
					matchSuccessBis, errorBis = match(ptMAttr, sAttr)
					if matchSuccess : 
						newTransfo = computeTransformationLaw(mAttr, sAttr)
						matching[mNeighbour][sNeighbour].append((newTransfo, error, it, 1))
						queue.append((mNeighbour, sNeighbour, newTransfo, transfo, 0))
						exploredSEdge.add(sEdge)
						mEdgesMatched.add(mEdge)
					elif matchSuccessBis: 
						newTransfo = computeTransformationLaw(mAttr, sAttr)
						matching[mNeighbour][sNeighbour].append((newTransfo, errorBis, it, 1))
						queue.append((mNeighbour, sNeighbour, newTransfo, previousTransfo, 0))
						exploredSEdge.add(sEdge)
						mEdgesMatched.add(mEdge)

		matchingValue = len(mEdgesMatched)
		if matchingValue > bestMatchingValue :
			print '\t\t New best matching found, value =', matchingValue, 'iter =', searchNumber
			bestMatching = matching
			bestMatchingValue = matchingValue
			bestSedgesExplo = exploredSEdge
			if matchingValue >= .95*nEdges : 
				break

	for k, v in bestMatching.iteritems() : 
		bestMatch = max(v.iteritems(), key = lambda (k2, v2) : (len(v2), -sum([e[1] for e in v2])))
		matchErr = 0
		matchN = 0
		for e in bestMatch[1] : 
			if e[-1] == 0 : 
				matchErr = 0
				matchN = 0
				break
			else : 
				matchErr += e[1]
				matchN += 1
		bestMatching[k]=(bestMatch[0], round(matchErr, 2), matchN)
	
	return bestMatching, bestSedgesExplo



"""
This function returns a list of basic matchings where it could
be a good idea to begin the search.  The list is ordered to begin
to explore the most promising basic matching.
A default parameter -limit=300 can be set to limits the number
of different basic matchings selected.  The assumption is made that the 
best matching should be found by considering the 300 most particular
basic matchings according the ordering (-mPerc, -sPerc, diff) where
	mPerc is the model edges percentage matched
	sPerc is the search edges percentage matched
	diff is the computed error of matching
"""
def getBasicMatchings(features, sGraph, limit=250, shuffleMode=False):
	orderedFeatures = models.orderedListOfKeys(features)
	from random import shuffle
	if shuffleMode : 
		shuffle(orderedFeatures)

	startingNodesConsidered = []
	basicMatchings = []

	#Scans the features=set of attributes
	for f in orderedFeatures :
		mAttrs = [dict(x) for x in f]
		#Scans the search space node to find nodes having similar features
		for sNode in sGraph.nodes_iter():
			sAttrs = [x[2] for x in sGraph.out_edges(sNode, data=True)]

			#Scans all the transformations law found that match mAttrs with sAttrs
			possibleTransformations = getTransformations(mAttrs, sAttrs, mPercTol=.6, sPercTol=.6)
			for (transfo, (mPerc, sPerc, diff)) in possibleTransformations :
				for mNode in features[f] :
					basicMatchings.append((mNode, sNode, transfo, (mPerc, sPerc, diff)))
					if mNode not in startingNodesConsidered : 
						startingNodesConsidered.append(mNode)
					if len(basicMatchings)>limit*4 : 
						if shuffleMode : 
							shuffle(basicMatchings)
							return basicMatchings[:limit]
						else : 
							return sorted(basicMatchings, key = lambda x : (-x[3][0], -x[3][1], x[3][2]))[:limit]

	if shuffleMode : 
		shuffle(basicMatchings)
		return basicMatchings[:limit]
	else : 
		return sorted(basicMatchings, key = lambda x : (-x[3][0], -x[3][1], x[3][2]))[:limit]



"""
Given two features, i.e two set of attributes, typically one
from the model and the other from the search space, this function
computes the possible transformations laws.  It returns a list of 
the possible transformation laws with, for each, a measure of the 
quality of the matching in terms of 
	percentage of the model attributes matched
	percentage of the search space attributes matched
	an error measuring the mismatching of some attributes
The list is sorted according these 3 criterias. 
"""
def getTransformations(mAttrs, sAttrs, mPercTol=.5, sPercTol=.5):
	transfos = []
	#Scans the model attributes
	for mAttr in mAttrs:
		mAttrsLeft = (list(mAttrs).remove(mAttr))
		#Scans the search attributes
		for sAttr in sAttrs:
			#Aligns the two attributes and compute the transformation law
			transfo = computeTransformationLaw(mAttr, sAttr)
			if transfo not in [x[0] for x in transfos] and (transfo['angle'] < 45 or transfo['angle'] > 315):
				matchValue = evaluateMatching(mAttrs, sAttrs, transfo, mPercTol, sPercTol)
				if matchValue != (-1, -1, -1) : 
					transfos.append((transfo, matchValue))
	
	return sorted(transfos, key = lambda x: (x[1][0], -x[1][2], x[1][1]), reverse = True)		



"""
Given two set of attributes -mAttrs and -sAttrs with a transformation
law -transfo, this function evaluates the matching between the two sets
according some tolerance.  If a good enough matching is found, the percentage
of matching are returned, else the tuple (-1, -1, -1) is returned.
"""
def evaluateMatching(mAttrs, sAttrs, transfo, mPercTol, sPercTol):
	matching = {}

	for mAttr in mAttrs:
		tAttr = transformAttributes(mAttr, transfo)	
		for sAttr in sAttrs:
			matchSuccess, error =  match(tAttr, sAttr)
			if matchSuccess :
				mAttrTuple = tuple(mAttr.values())
				if mAttrTuple in matching : 
					if error < matching[mAttrTuple][1] : 
						matching[mAttrTuple] = (tuple(sAttr.values()), error)
				else : 
					matching[mAttrTuple] = (tuple(sAttr.values()), error)

	errorGlo = sum([x[1] for x in matching.values()])
	nMatched = len(matching)

	matchValue = (round(nMatched/float(len(mAttrs)), 2), round(nMatched/float(len(sAttrs)), 2), round(errorGlo, 2))
	if matchValue[0] > mPercTol and matchValue[1] > sPercTol : 
		return matchValue
	else : 
		return (-1, -1, -1)



"""
This function tries to match two attributes.  It compares them 
according some tolerances on the length (percentage) and angle (degree)
and returns a boolean value telling if a match have been found and an error.
"""
def match(attr1, attr2, lengthTolPercentage=.33, angleTol=25, lengthWeight=2, angleWeight=1):
	lengthTol=lengthTolPercentage*min(attr1['length'], attr2['length'])
	lengthDiff = abs(attr1['length'] - attr2['length'])
	angleDiff = abs(attr1['angle'] - attr2['angle'])
	if angleDiff > 180 : angleDiff = 360 - angleDiff

	match = False if lengthDiff > lengthTol or angleDiff > angleTol else True
	error = round(lengthDiff*lengthWeight + angleDiff*angleWeight, 2)
	
	return match, error	



"""
Given two attributes -attr1 and -attr2, this function
computes and returns the transformation rule the go 
from -attr1 to -attr2 so that there are perfectly aligned
"""
def computeTransformationLaw(attr1, attr2):
	transfo = {}
	transfo['length']	=	round(attr2['length']/float(attr1['length']), 2)
	#transfo['angle']	=	180 - abs(abs(attr1['angle'] - attr2['angle']) - 180)
	transfo['angle']	=	(attr2['angle']-attr1['angle']+360)%360
	return transfo



"""
Given an attribute -attr and a transformation law -transfo, 
this function transforms the -attr according the -transfo
and returns it.
"""
def transformAttributes(attr, transfo):
	transformedAttribute = {}
	transformedAttribute['length']	=	attr['length']*transfo['length']
	transformedAttribute['angle']	=	(attr['angle']+transfo['angle'])%360
	return transformedAttribute

