import math
import networkx as nx
from math import *
from thinning import guo_hall_thinning as ght
from collections import defaultdict
from utils import dist
import numpy as np
import cv2

"""
This function performs a thinning on the source image -src
and return the result.  A thinning aims at shrinking every
strokes on the image to be 1 pixel thick only.
The Guo Hall scheme is here in this function
"""
def guo_hall_thinning(src):
	return ght(src.copy()) 

"""
This function performs a thinning on the source image -src
and return the result.  A thinning aims at shrinking every
strokes on the image to be 1 pixel thick only.
The Zhang Suen scheme is here in this function
Source link : 
https://github.com/bsdnoobz/zhang-suen-thinning
"""
from scipy import weave
def zhang_suen_thinning(src):

	dst = src.copy() / 255
	prev = np.zeros(src.shape[:3], np.uint8)
	diff = None

	while True:
		dst = _thinningIteration(dst, 0)
		dst = _thinningIteration(dst, 1)
		diff = np.absolute(dst - prev)
		prev = dst.copy()
		if np.sum(diff) == 0:
			break
	return dst * 255
def _thinningIteration(im, iter):
	I, M = im, np.zeros(im.shape, np.uint8)
	expr = """
	for (int i = 1; i < NI[0]-1; i++) {
		for (int j = 1; j < NI[1]-1; j++) {
			int p2 = I2(i-1, j);
			int p3 = I2(i-1, j+1);
			int p4 = I2(i, j+1);
			int p5 = I2(i+1, j+1);
			int p6 = I2(i+1, j);
			int p7 = I2(i+1, j-1);
			int p8 = I2(i, j-1);
			int p9 = I2(i-1, j-1);
			int A  = (p2 == 0 && p3 == 1) + (p3 == 0 && p4 == 1) +
					 (p4 == 0 && p5 == 1) + (p5 == 0 && p6 == 1) +
					 (p6 == 0 && p7 == 1) + (p7 == 0 && p8 == 1) +
					 (p8 == 0 && p9 == 1) + (p9 == 0 && p2 == 1);
			int B  = p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
			int m1 = iter == 0 ? (p2 * p4 * p6) : (p2 * p4 * p8);
			int m2 = iter == 0 ? (p4 * p6 * p8) : (p2 * p6 * p8);
			if (A == 1 && B >= 2 && B <= 6 && m1 == 0 && m2 == 0) {
				M2(i,j) = 1;
			}
		}
	} 
	"""

	weave.inline(expr, ["I", "iter", "M"])
	return (I & ~M)

"""
Given a skeleton -skel with strokes 1-pixel thick only, 
this function will detects pixels that can be considered
as nodes.  It returns a diGraph containing the nodes.
"""
def node_detection(skel):
	def is_node_1_neighborhood(x, y, skel):
		accept_pixel_as_node = False
		item = skel.item
		p2 = item(x - 1, y) / 255
		p3 = item(x - 1, y + 1) / 255
		p4 = item(x, y + 1) / 255
		p5 = item(x + 1, y + 1) / 255
		p6 = item(x + 1, y) / 255
		p7 = item(x + 1, y - 1) / 255
		p8 = item(x, y - 1) / 255
		p9 = item(x - 1, y - 1) / 255

		A = (p2 == 0 and p3 == 1) + (p3 == 0 and p4 == 1) + \
			(p4 == 0 and p5 == 1) + (p5 == 0 and p6 == 1) + \
			(p6 == 0 and p7 == 1) + (p7 == 0 and p8 == 1) + \
			(p8 == 0 and p9 == 1) + (p9 == 0 and p2 == 1)

		if (A >= 3) or (A == 1):
			accept_pixel_as_node = True
		return accept_pixel_as_node

	def is_node_2_neighborhood(x, y, skel):
		""" checks the number of components around a pixel.
		if it is either 1 or more than 3, it is a node
				p12 p13 p14
			p11 p3  p4  p5  p15 
			p10 p2  p1  p6  p16
			p21 p9  p8  p7  p17
				p20 p19 p18 
		"""
		accept_pixel_as_node = False
		item = skel.item
		p2 = item(x - 1, y) / 255
		p3 = item(x - 1, y + 1) / 255
		p4 = item(x, y + 1) / 255
		p5 = item(x + 1, y + 1) / 255
		p6 = item(x + 1, y) / 255
		p7 = item(x + 1, y - 1) / 255
		p8 = item(x, y - 1) / 255
		p9 = item(x - 1, y - 1) / 255
		p10 = item(x-2,y) / 255	
		p11 = item(x-2,y+1) / 255
		p12 = item(x-1,y+2) / 255
		p13 = item(x,y+2) / 255
		p14 = item(x+1,y+2) / 255
		p15 = item(x+2,y+1) / 255
		p16 = item(x+2,y) / 255
		p17 = item(x+2,y-1) / 2
		p18 = item(x+1,y-2) / 255
		p19 = item(x,y-2) / 255
		p20 = item(x,y) / 255

		if (((p6 ==1 and p16 ==1) and ((p8==1 and p19==1) or (p4==1 and p13==1))) or
			((p10 ==1 and p2 ==1) and ((p8==1 and p19==1) or (p4==1 and p13==1)))) :
			accept_pixel_as_node = True
		
		return accept_pixel_as_node


	graph = nx.DiGraph()
	h, w = skel.shape
	item = skel.item

	for x in xrange(1,w-2):
		for y in xrange(1,h-2):
			if item(y, x) != 0 and is_node_1_neighborhood(y, x, skel):
				graph.add_node((x, y), level=1)
			# elif is_node_2_neighborhood(y, x, skel):
			# 	graph.add_node((x, y), level=2)
	return graph



"""
Given a skeleton -skel with strokes 1-pixel thick only and
a digraph -graph containing some nodes, this function
will detects the path between the nodes and in that way
adds edges to the digraph.
"""
def edge_detection(skel, graph):
	#yields on the skeleton the 1-neighborhood of a pixel
	def neighbors((x, y)):
		item = skel.item
		height, width = skel.shape
		for dy in [-1, 0, 1]:
			for dx in [-1, 0, 1]:
				if (dx != 0 or dy != 0) and 1 <= x + dx < width-1 and 1 <= y + dy < height-1 and item(y + dy, x + dx) != 0:
					yield x + dx, y + dy


	#initialization of the BFSs : 
	label = 1 
	label_node = dict() #map to each label the starting node

	label_length = defaultdict(int)
	label_pixels = defaultdict(list)
	label_npixels = defaultdict(int)
	pixel_label = defaultdict(int) #pixel_label == 0 -> not visited yet, -1 --> node

	queues = []
	edges = set()

	sqrt2 = sqrt(2)
	#initialization of queue
	for x, y in graph.nodes_iter():
		pixel_label[x,y] = -1
		for a, b in neighbors((x, y)):
			if (a,b) in graph.nodes_iter():
				edges.add(((x,y), (a,b), -1, -1))
				#addEdge x,y to a,b
			else:
				label_node[label] = (x, y)
				label_length[label] = sqrt2 if abs(x - a) == 1 and abs(y - b) == 1 else 1
				#append a new BFS search for the node x,y starting at a,b
				queues.append((label, (x, y), (a, b)))
				label_pixels[label].append((x, y))
			label += 1
	
	#perform all the searches
	time = 0
	while queues:
		time+=1
		new_queues = []
		for label, (px, py), (nx, ny) in queues:
			plabel = pixel_label[(nx, ny)]
			if plabel == 0:
				label_pixels[label].append((nx, ny))
				label_length[label] += sqrt2 if abs(nx - px) == 1 and abs(ny - py) == 1 else 1
				#start a new searches from the newly labeled node onto its neighbors
				pixel_label[(nx, ny)] = label

				for nnx, nny in neighbors((nx, ny)):
					if (nnx, nny) not in graph and (nnx, nny) != (px,py):
						new_queues.append((label, (nx, ny), (nnx, nny)))
			elif plabel != label:
				l1 = min(label, plabel)
				l2 = max(label, plabel)
				if l1 == -1 and l2== -1:
					edges.add((u,v,-1,-1))
				else:
					u, v = label_node[l1], label_node[l2]
					edges.add((u,v,l1,l2))

		queues = new_queues

	for u,v, l1, l2 in edges:
		if l1!=-1 and l2!=-1:
			# u,v = label_node[l1], label_node[l2]
			if u != v and u not in neighbors(v) and v not in neighbors(u):
				new = list(label_pixels[l2])
				new.reverse()
				length = label_length[l1] + label_length[l2]
				
				pixels = np.array((label_pixels[l1]+ new))
				res = cv2.approxPolyDP(pixels, 0.1*length, False)
				last = (res[0][0][0], res[0][0][1])
				idx = 0
				for node in res[1:]:
					newNode = (node[0][0], node[0][1])
					newIdx = np.argwhere(np.all(pixels==node,axis=1))
					if newNode not in graph:
						graph.add_node(newNode, level=2)
					u = newNode
					v = last
					length = cv2.arcLength(pixels[idx:newIdx],False)
					graph.add_edge(u, v,
					   angle= int(atan2(u[1]-v[1], v[0]-u[0])/pi*180)%360,
					   length=length)
					graph.add_edge(v, u,
					   angle= int(atan2(v[1]-u[1], u[0]-v[0])/pi*180)%360,
					   length=length)
					last = newNode
					idx = newIdx



"""
Given a detected graph -graph and a tolerance of merging
-mergeDist, this function will analyse the -graph and merges 
together the nodes that are too close according the tolerance
"""
def merge_close_node_filter(graph, mergeDist):
	to_remove = set()
	#could improve with lazy generation? or sorted by closeness
	for n1, attr1 in graph.nodes_iter(data=True):
		if n1 in to_remove:
			continue
		for n2 in [x for x,attr in graph.nodes_iter(data=True) if dist(x,n1)<mergeDist and n1 != x]:
			graph.add_node(n1,{'level':3})
			to_remove.add(n2)
			for _,v,attr in graph.out_edges(n2, data=True):
				if (n1,v) not in graph.out_edges(n1) and n1!=v:
					graph.add_edge(n1,v,attr)
			for v,_,attr in graph.in_edges(n2, data=True):
				if (v,n1) not in graph.out_edges(v) and  n1!=v:
					graph.add_edge(v,n1,attr)
	#print("merge_close_node_filter : "+str(len(to_remove))+" nodes removed")
	graph.remove_nodes_from(to_remove)



"""
The graph -graph in argument may contains a lot of connected 
components.  Some of them may be very small and considered as 
noise according a minimum number of nodes -minNode.  This function
will filter out every connected components that are too small.
"""
def connected_components_filter(graph, minNode):
    connected_components = sorted(list(nx.connected_component_subgraphs(graph.to_undirected())), key = lambda graph: graph.number_of_nodes())
    for subgraph in connected_components:
        if subgraph.number_of_nodes() < minNode:
        	graph.remove_nodes_from(subgraph)