import networkx as nx
from math import *
from collections import defaultdict
import pprint as pp

"""
This function creates a model for the sudoku problem.  By default, 
a 9 by 9 sudoku is created but it can be changed with the parameter -n
"""
def sudokuModel(n = 9) :
	sudoku = nx.DiGraph() 
	nodes = ((i, j) for i in range(n+1) for j in range(n+1))
	sudoku.add_nodes_from(nodes)
	for x, y in sudoku.nodes_iter():
		neighbors = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)] #this is potential neighbours
		for px, py in neighbors:
			if sudoku.has_node((px, py)): #only real neighbours are added
				angle = int(atan2(y-py, px-x)/pi*180)%360
	 			sudoku.add_edge((x,y), (px, py), {'length':1, 'angle':angle}) 

	return sudoku

"""
This function creates a model for the pyramid math problem.  By default, 
a depth 9 pyramid is created but it can be changed with the parameter -n
"""
def pyramidMathModel(n=9) : 
	pyramidMath = nx.DiGraph()
	for y in range(n) : 
		for x in range(n-y) : 
			currentCell = (x,y)
			
			bottomLeft = (y+2*x, 2*y)
			bottomCenter = (bottomLeft[0]+1, bottomLeft[1])
			bottomRight = (bottomLeft[0]+2, bottomLeft[1])
			topLeft = (bottomLeft[0], bottomLeft[1]+2)
			topCenter = (bottomLeft[0]+1, bottomLeft[1]+2)
			topRight = (bottomLeft[0]+2, bottomLeft[1]+2)

			#add edges at the bottom of the cell
			if y == 0 : 
				pyramidMath.add_edge(bottomLeft, bottomRight, {'length':2, 'angle':0})
				pyramidMath.add_edge(bottomRight, bottomLeft, {'length':2, 'angle':180})
			else : 
				pyramidMath.add_edge(bottomLeft, bottomCenter, {'length':1, 'angle':0})
				pyramidMath.add_edge(bottomCenter, bottomRight, {'length':1, 'angle':0})
				pyramidMath.add_edge(bottomRight, bottomCenter, {'length':1, 'angle':180})
				pyramidMath.add_edge(bottomCenter, bottomLeft, {'length':1, 'angle':180})
			#add the vertical edges
			pyramidMath.add_edge(bottomLeft, topLeft, {'length':2, 'angle':90})
			pyramidMath.add_edge(topLeft, bottomLeft, {'length':2, 'angle':270})
			pyramidMath.add_edge(bottomRight, topRight, {'length':2, 'angle':90})
			pyramidMath.add_edge(topRight, bottomRight, {'length':2, 'angle':270})
			#add the top edge for the top cell
			if y == n-1 : 
				pyramidMath.add_edge(topLeft, topRight, {'length':2, 'angle':0})
				pyramidMath.add_edge(topRight, topLeft, {'length':2, 'angle':180})
			#add the top edges for the beginning and ending of a row
			if x == 0 and x != n-y and y != n-1: 
				pyramidMath.add_edge(topLeft, topCenter, {'length':1, 'angle':0})
				pyramidMath.add_edge(topCenter, topLeft, {'length':1, 'angle':180})
			if x == n-y-1 and x != 0 and y != n-1: 
				pyramidMath.add_edge(topCenter, topRight, {'length':1, 'angle':0})
				pyramidMath.add_edge(topRight, topCenter, {'length':1, 'angle':180})
			if y == n-1 : 
				pyramidMath.add_edge(topLeft, topRight, {'length':2, 'angle':0})
				pyramidMath.add_edge(topRight, topLeft, {'length':2, 'angle':1800})
	return pyramidMath

"""
Given a -problemType and a -size, this function returns the corresponding
problem model
"""
def getModel(problemType, size=9) : 
	if problemType == 'sudoku' : 
		return sudokuModel(size)
	elif problemType == 'pyramidMath' : 
		return pyramidMathModel(size)

"""
This function scans the model to output a dictionary
of key=feature : value=listOfNodes pair.
A feature is a set of attributes of a node and
the value that corresponds is the list of nodes having this
feature/set of attributes.
"""
def getFeatures(model) :
	dico = defaultdict(list)
	for node in model:
		key = tuple(sorted([tuple(edge[2].items()) for edge in model.out_edges(node, data=True)]))
		dico[key].append(node)
	return dico



"""
Given a dictionary, this function returns a list of 
the keys ordered in decreasing order.
"""
def orderedListOfKeys(dico) :
	return sorted(dico, key=lambda k: len(dico[k]), reverse=False)


"""
This function performs the propagate step of 
"""
def propagate(sudoku) : 
	loop = True

	while loop :
		unbound = 0
		unboundCell = None
		loop = False
		rawsData = [set() for i in range(9)]
		columnsData = [set() for i in range(9)]
		squaresData = [[set() for i in range(3)] for j in range(3)]
		
		for i, raw in enumerate(sudoku) : 
			for j, cell in enumerate(raw) : 
				if type(cell) is int : 
					rawsData[i].add(cell)
					columnsData[j].add(cell)
					squaresData[i/3][j/3].add(cell)

		for i, raw in enumerate(sudoku) : 
			for j, cell in enumerate(raw) : 
				if type(cell) is set : 
					unbound += 1
					for e in rawsData[i] : 
						if e in cell : cell.remove(e)
					for e in columnsData[j] : 
						if e in cell : cell.remove(e)
					for e in squaresData[i/3][j/3] : 
						if e in cell : cell.remove(e)
					
					if len(cell) == 1 : 
						sudoku[i][j] = cell.pop()
						rawsData[i].add(sudoku[i][j])
						columnsData[j].add(sudoku[i][j])
						squaresData[i/3][j/3].add(sudoku[i][j])
						loop = True
						unbound -= 1
					elif len(cell) == 0 : 
						#print 'error !! (', i, ',', j, ')', cell
						#pp.pprint(rawsData)
						#pp.pprint(columnsData)
						#pp.pprint(squaresData)
						return -1
					else : 
						unboundCell = (i,j)
	return unboundCell if unbound>0 else 0



"""
"""
def sudokuCopy(sudoku) : 
	out = [ [ sudoku[k][j] if (type(sudoku[k][j]) is int) else set(sudoku[k][j]) for j in range(9)] for k in range(9)]
	return out



"""
"""
def sudokuSolver(sudoku, s) : 
	#print s, 'Propagating...'
	result = propagate(sudoku)
	#print s, 'Propagating done ! Result is', result, ' ! \n'
	if (result == -1) :
		return -1
	elif (result == 0) : 
		return sudoku
	else : 
		#print s, 'with dom ', sudoku[result[0]][result[1]]
		#print s, 'Current sudoku is'
		#pp.pprint(sudoku)
		#print '\n'

		(x,y) = result
		for val in sudoku[x][y] : 
			#print s, 'Attributing value', val, 'to cell', x, ',', y
			newSudoku = sudokuCopy(sudoku)
			newSudoku[x][y] = val
			nextIter = sudokuSolver(newSudoku, s + '\t')
			if nextIter == -1 : 
				#print s, 'Bactracking and checking next value...'
				continue
			else : 
				#pp.pprint(nextIter)
				return nextIter
		return -1




"""
"""
def sudokuResolution(sudokuDict) :
	rawSudoku = [[ set(i for i in range(1,10,1)) if (k+1,j+1) not in sudokuDict else sudokuDict[(k+1,j+1)] for j in range(9)] for k in range(9)]
	sol = sudokuSolver(rawSudoku, '')

	return sol