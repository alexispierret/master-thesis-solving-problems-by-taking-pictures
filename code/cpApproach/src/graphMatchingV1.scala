import java.io.{File, PrintWriter, FileReader, BufferedReader}
import java.util.Scanner
import oscar.cp.core.NoSolutionException

import scala.math._
import oscar.cp._

import scala.collection.mutable._
import scala.util.Random

object graphMatchingV1 extends App with CPModel{

  val (modelPoints2Index, modelIndex2Points, modelEdges, modelNodes) = readFile(args(0))
  val nModelNodes = modelPoints2Index.size
  val nModelEdges = modelEdges.size
  val (spPoints2Index, spIndex2Points, spEdges, spNodes) = readFile(args(1))
  val nSPNodes = spPoints2Index.size
  val nSPEdges = spEdges.size

  println(nModelNodes)
  println("modelIndex2Points " + modelIndex2Points)
  println("modelEdges " + modelEdges)
  println(nSPNodes)
  println("spIndex2Points " + spIndex2Points)
  println("spEdges " + spEdges)
  println





  //***---Computes nodes kinds---***
  def computeKind(indexNode : Int, nodes : Map[Int, Set[Int]],  edges : Set[(Int, Int)]) : (Int, Int, Int, Int, Int) = {
    val oneNeighbourhood = nodes(indexNode)
    val nOneNeighbourhood = oneNeighbourhood.size

    val twoNeighbourhood : Set[Int] = Set()
    for (elem <- oneNeighbourhood){
      twoNeighbourhood ++= nodes(elem)
    }

    twoNeighbourhood -= indexNode
    val nTwoNeighbourhood = twoNeighbourhood.size

    var oneToOneConnections = 0
    var oneToTwoConnections = 0
    var twoToTwoConnections = 0
    for (e <- edges){
      val (from, to) = e
      if (oneNeighbourhood.contains(from)) {
        if (oneNeighbourhood.contains(to) && from < to)
          oneToOneConnections += 1
        if (twoNeighbourhood.contains(to))
          oneToTwoConnections += 1
      }
      if (twoNeighbourhood.contains(from) && twoNeighbourhood.contains(to) && from < to)
        twoToTwoConnections += 1
    }

    (nOneNeighbourhood, nTwoNeighbourhood, oneToOneConnections, oneToTwoConnections, twoToTwoConnections)
  }

  def compareKinds(k1 : (Int, Int, Int, Int, Int), k2 : (Int, Int, Int, Int, Int)) : Int = {
    val degDiff = abs(k1._1 - k2._1)
    val deg2Diff = abs(k1._2 - k2._2)
    val conn1to1Diff = abs(k1._3 - k2._3)
    val conn1to2Diff = abs(k1._4 - k2._4)
    val conn2to2Diff = abs(k1._5 - k2._5)

    if (degDiff > 1 || deg2Diff > 1 || conn1to1Diff > 1 || conn1to2Diff > 2 || conn2to2Diff > 1)
      return -1
    //else
    return degDiff + deg2Diff + conn1to1Diff + conn1to2Diff + conn2to2Diff
  }

  val modelIndex2Kind : Map[Int, (Int, Int, Int, Int, Int)] = Map()
  val modelKind2Index : Map[(Int, Int, Int, Int, Int), ListBuffer[Int]] = Map()
  val modelKinds : Set[(Int, Int, Int, Int, Int)] = Set()
  for (mIndex <- modelNodes.keys){
    val modelKind = computeKind(mIndex, modelNodes, modelEdges)
    modelIndex2Kind(mIndex) = modelKind
    if (!(modelKinds contains modelKind))
      modelKind2Index(modelKind) = ListBuffer()
    modelKinds += modelKind
    modelKind2Index(modelKind) += mIndex
  }

  for ( (kind, indices) <- modelKind2Index)
    println("kind " + kind + " corresponds to nodes " + indices.map(modelIndex2Points(_)))

  val variablesOrdering = modelKind2Index.toArray.sortBy(x => (x._2.size, x._1)).map(_._2).flatten
  val startingVariables = variablesOrdering.take(5)
  println(variablesOrdering.mkString(" "))
  println


  val modelKind2Domain : Map[(Int, Int, Int, Int, Int), ListBuffer[(Int, Int)]] = Map()
  for (k <- modelKinds) modelKind2Domain += ((k, ListBuffer()))
  for (sIndex <- spNodes.keys) {
    val sKind = computeKind(sIndex, spNodes, spEdges)
    for (mKind <- modelKinds){
      val error = compareKinds(mKind, sKind)
      if (error >= 0)
        modelKind2Domain(mKind) += ((sIndex, error))
    }
  }
  for (k <- modelKinds) modelKind2Domain(k) = modelKind2Domain(k).sortWith(_._2 < _._2)






  //***---Declaring the VARIABLES---***
  val domain = -1 until nSPNodes
  val ind = modelKind2Domain(modelIndex2Kind(2)).map(a => a._1).toSeq
  val modelVars = for (i <- 0 until nModelNodes) yield CPIntVar(modelKind2Domain(modelIndex2Kind(i)).map(a => a._1).toSeq:+(-1))
  val nViolationsAllowed = max((0.4*nModelNodes).toInt, nModelNodes-nSPNodes)
  val valueOcc: Array[(Int, CPIntVar)] = (for (i <- -1 until nSPNodes) yield {if (i == -1) (-1, CPIntVar(0 to nViolationsAllowed)) else (i, CPIntVar(0 to 1))}).toArray







  //***---Declaring the CONSTRAINTS---***
  //Global Cardinality Constraint
  add(gcc(modelVars, valueOcc))

  //Edges constraints
  val dummyXtoEachNode = for (node <- domain.toList) yield (-1, node) //dummyXtoDummyY considered
  val eachNodeToDummyY = for (node <- domain.toList if node != -1) yield (node, -1)
  val tableValues = eachNodeToDummyY:::dummyXtoEachNode:::spEdges.toList
  for (i <- 0 until nModelNodes; j <- i+1 until nModelNodes){
    if (modelEdges.contains((i,j))) {
      add(table(modelVars(i), modelVars(j), tableValues))
    }
  }





  //****----SEARCH BLOCK----****
  var lastVarBound = -1
  val rand = new Random
  search {
    val notBound = (0 until nModelNodes).filter(!modelVars(_).isBound).toSet
    if (notBound.isEmpty)
      noAlternative
    else if (notBound.size == nModelNodes) {
      lastVarBound = startingVariables(rand.nextInt(5))
      println("first variable selected " + modelIndex2Points(lastVarBound))
      val domainToBranch = modelKind2Domain(modelIndex2Kind(lastVarBound)).filter(x => modelVars(lastVarBound).toArray.contains(x._1)).map(_._1)
      branchAll(domainToBranch)(value => add(modelVars(lastVarBound) == value))
    }
    else {
      assert(lastVarBound != -1)
      val neighbours = modelNodes(lastVarBound)
      val intersection = neighbours.intersect(notBound).toVector
      if (intersection.isEmpty) {
        val i = notBound.minBy(modelVars(_).size)
        lastVarBound = i
        val domainToBranch = modelKind2Domain(modelIndex2Kind(lastVarBound)).filter(x => modelVars(lastVarBound).toArray.contains(x._1)).map(_._1)
        branchAll(domainToBranch)(v => add(modelVars(i)==v))
      }
      else {
        val i = intersection(rand.nextInt(intersection.size))
        lastVarBound = i
        val domainToBranch = modelKind2Domain(modelIndex2Kind(lastVarBound)).filter(x => modelVars(lastVarBound).toArray.contains(x._1)).map(_._1)
        branchAll(domainToBranch)(v => add(modelVars(i)==v))
      }
    }
  }




  //****----ON SOLUTION BLOCK----*****
  var bestSol = Array.fill(nModelNodes)(-1)
  var bestVal = Int.MaxValue
  onSolution {
    bestSol = modelVars.map(_.value)
    bestVal = valueOcc(0)._2.value
  }




  //****----LAUNCH SEARCH----****
  val startTime = System.currentTimeMillis()

  //<<-- Minimize the number of unbound modelVars -->>
  minimize(valueOcc(0)._2)

  //<<-- Find a first solution for the LNS -->>
  try{
    start(nSols = 1)
    println("First sol found after " + (System.currentTimeMillis()-startTime) + " ms")
  }
  catch {
    case e : NoSolutionException => {
      println("No Solution")
      System.exit(0)
    }
  }


  //<<-- LNS LOOP -->>
  var fragmentSize = 50.0
  while (bestVal != 0 && System.currentTimeMillis()-startTime < 100*1000){
    val selectedVars : ListBuffer[Int] = ListBuffer()
    val stat = startSubjectTo(failureLimit = 350){
      (0 until nModelNodes).filter(k => !(rand.nextInt(100) < fragmentSize)).foreach(i => {
        add(modelVars(i) == bestSol(i))
        selectedVars += i
      })
      if (selectedVars.size > 0){
        lastVarBound = selectedVars(rand.nextInt(selectedVars.size))
        println("selectedVars is " + selectedVars.size)
      }
      else {
        lastVarBound = -1
        println("NO VAR SELECTED !!!!!!")
        val notBound = (0 until nModelNodes).filter(!modelVars(_).isBound).toSet
        println(notBound.mkString(" "))
      }

    }

    if (stat.completed)
      fragmentSize = min(100, fragmentSize * 1.1)
    else
      fragmentSize = max(15, fragmentSize * .9)

    println("New fragmentSize is " + fragmentSize)
  }





  //<<-- Termination -->>
  val writer = new PrintWriter(new File("../copOut.txt" ))
  writer.write("{\n")
  for (i <- 0 until nModelNodes)
    if (bestSol(i) >= 0)
      if (i == nModelNodes-1){
        writer.write(modelIndex2Points(i) + ":" + spIndex2Points(bestSol(i)) + "\n")
        println(modelIndex2Points(i) + " : " + spIndex2Points(bestSol(i)))
      }
      else{
        writer.write(modelIndex2Points(i) + ":" + spIndex2Points(bestSol(i)) + ",\n")
        println(modelIndex2Points(i) + " : " + spIndex2Points(bestSol(i)))
      }
    else
      println(modelIndex2Points(i) + " : " + bestSol(i))

  writer.write("}")
  writer.close()

  println("\nSearch ended after " + (System.currentTimeMillis()-startTime)/1000 + "s")
  println("bestVal is " + bestVal)




  def readFile (fileName : String) : (Map[(Int,Int), Int], Map[Int, (Int, Int)], Set[(Int, Int)], Map[Int, Set[Int]]) = {
    val sc = new Scanner(new BufferedReader(new FileReader(fileName)))

    val points2index = Map[(Int,Int), Int]()
    val index2points = Map[Int, (Int,Int)]()
    val edges = Set[(Int, Int)]()
    var nodes = Map[Int, Set[Int]]()

    var pIndex = 0
    val pattern = "\\(([0-9]+), ([0-9]+)\\) \\(([0-9]+), ([0-9]+)\\) .*".r

    while (sc.hasNext) {
      try {
        val line = sc.nextLine()
        val pattern(x1,x2,y1,y2) = line

        val x = (x1.toInt, x2.toInt)
        val y = (y1.toInt, y2.toInt)
        if (!points2index.contains(x)) {
          points2index.+=((x, pIndex))
          index2points.+=((pIndex, x))
          nodes+=((pIndex, Set()))
          pIndex+=1
        }
        if (!points2index.contains(y)) {
          points2index.+=((y, pIndex))
          index2points.+=((pIndex, y))
          nodes.+=((pIndex, Set()))
          pIndex+=1
        }

        val edge: (Int, Int) = (points2index(x), points2index(y))
        edges += edge
        nodes(points2index(x)) += points2index(y)
      }
      catch {
        case e: Exception =>
      }
    }

    (points2index, index2points, edges, nodes)
  }
}
