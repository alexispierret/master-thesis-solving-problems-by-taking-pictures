import java.io.{File, PrintWriter, FileReader, BufferedReader}
import java.util.Scanner
import oscar.cp.core.NoSolutionException

import scala.math._
import oscar.cp._

import scala.collection.mutable._
import scala.util.Random

object graphMatchingV2 extends App with CPModel{

  val (modelNode2Index, modelIndex2Node, modelEdge2Index, modelIndex2Edge, modelNodes) = readFile(args(0))
  val nModelNodes = modelNode2Index.size
  val nModelEdges = modelEdge2Index.size
  val (searchNode2Index, searchIndex2Node, searchEdge2Index, searchIndex2Edge, searchNodes) = readFile(args(1))
  val nSearchNodes = searchNode2Index.size
  val nSearchEdges = searchEdge2Index.size

  println(nModelNodes)
  println("modelIndex2Points " + modelIndex2Node)
  println("modelEdges " + modelEdge2Index)
  println(nSearchNodes)
  println("spIndex2Points " + searchIndex2Node)
  println("spEdges " + searchEdge2Index)
  println





  //***---Computes nodes kinds---***
  def computeKind(indexNode : Int, nodes : Map[Int, Set[Int]],  edges : Map[(Int, Int), Int]) : (Int, Int, Int, Int, Int) = {
    val oneNeighbourhood = nodes(indexNode)
    val nOneNeighbourhood = oneNeighbourhood.size

    val twoNeighbourhood : Set[Int] = Set()
    for (elem <- oneNeighbourhood){
      twoNeighbourhood ++= nodes(elem)
    }

    twoNeighbourhood -= indexNode
    val nTwoNeighbourhood = twoNeighbourhood.size

    var oneToOneConnections = 0
    var oneToTwoConnections = 0
    var twoToTwoConnections = 0
    for (e <- edges.keys){
      val (from, to) = e
      if (oneNeighbourhood.contains(from) && oneNeighbourhood.contains(to))
        oneToOneConnections += 1
      if ((oneNeighbourhood.contains(from) && twoNeighbourhood.contains(to)) || (oneNeighbourhood.contains(to) && twoNeighbourhood.contains(from)))
        oneToTwoConnections += 1
      if (twoNeighbourhood.contains(from) && twoNeighbourhood.contains(to))
        twoToTwoConnections += 1
    }

    (nOneNeighbourhood, nTwoNeighbourhood, oneToOneConnections, oneToTwoConnections, twoToTwoConnections)
  }
  def compareKinds(k1 : (Int, Int, Int, Int, Int), k2 : (Int, Int, Int, Int, Int)) : Int = {
    val degDiff = abs(k1._1 - k2._1)
    val deg2Diff = abs(k1._2 - k2._2)
    val conn1to1Diff = abs(k1._3 - k2._3)
    val conn1to2Diff = abs(k1._4 - k2._4)
    val conn2to2Diff = abs(k1._5 - k2._5)

    if (degDiff > 1 || deg2Diff > 1 || conn1to1Diff > 1 || conn1to2Diff > 2 || conn2to2Diff > 1)
      return -1
    //else
    return degDiff + deg2Diff + conn1to1Diff + conn1to2Diff + conn2to2Diff
  }

  val modelIndex2NodeKind : Map[Int, (Int, Int, Int, Int, Int)] = Map()
  val modelNodeKind2Index : Map[(Int, Int, Int, Int, Int), ListBuffer[Int]] = Map()
  val modelNodeKinds : Set[(Int, Int, Int, Int, Int)] = Set()
  //Computations of modelIndex2NodeKind, modelNodeKind2Index and modelNodeKinds
  for (mIndex <- modelNodes.keys){
    val modelKind = computeKind(mIndex, modelNodes, modelEdge2Index)
    modelIndex2NodeKind(mIndex) = modelKind
    if (!(modelNodeKinds contains modelKind))
      modelNodeKind2Index(modelKind) = ListBuffer()
    modelNodeKinds += modelKind
    modelNodeKind2Index(modelKind) += mIndex
  }

  /*for ( (kind, indices) <- modelNodeKind2Index)
    println("kind " + kind + " corresponds to nodes " + indices.map(modelIndex2Node(_)))
  */
  
  val modelIndex2EdgeKind : Map[Int, ((Int, Int, Int, Int, Int), (Int, Int, Int, Int, Int))] = Map()
  val modelEdgeKind2Index : Map[((Int, Int, Int, Int, Int), (Int, Int, Int, Int, Int)), ListBuffer[Int]] = Map()
  val modelEdgeKinds : Set[((Int, Int, Int, Int, Int), (Int, Int, Int, Int, Int))] = Set()
  //Compuation of modelIndex2EdgeKind, modelEdgeKind2Index and modelEdgeKinds
  for ( (mEdgeIndex,(fromIndex, toIndex)) <- modelIndex2Edge){
    val fromKind = modelIndex2NodeKind(fromIndex)
    val toKind = modelIndex2NodeKind(toIndex)
    val edgeKindList = List(fromKind, toKind).sorted
    val edgeKind = (edgeKindList(0), edgeKindList(1))
    modelIndex2EdgeKind(mEdgeIndex) = edgeKind
    if (!(modelEdgeKinds contains edgeKind))
      modelEdgeKind2Index(edgeKind) = ListBuffer()    
    modelEdgeKinds += edgeKind
    modelEdgeKind2Index(edgeKind) += mEdgeIndex
  }

  
  val modelEdgeKind2Domain : Map[ ((Int, Int, Int, Int, Int), (Int, Int, Int, Int, Int)), ListBuffer[(Int, Int)]] = Map()
  //Compuation of modelEdgeKind2Domain
  for (k <- modelEdgeKinds) modelEdgeKind2Domain += ((k, ListBuffer()))
  for ( (sEdgeIndex, (sFromIndex, sToIndex)) <- searchIndex2Edge){
    val sFromKind = computeKind(sFromIndex, searchNodes, searchEdge2Index)
    val sToKind = computeKind(sToIndex, searchNodes, searchEdge2Index)
    val searchEdgeKindList = List(sFromKind, sToKind).sorted
    val searchEdgeKind = (searchEdgeKindList(0), searchEdgeKindList(1))

    println("searchEdge " + (searchIndex2Node(searchIndex2Edge(sEdgeIndex)._1), searchIndex2Node(searchIndex2Edge(sEdgeIndex)._2)) + " has kind " + searchEdgeKind)

    for (mEdgeKind <- modelEdgeKinds){
      val error1 = compareKinds(mEdgeKind._1, searchEdgeKind._1)
      val error2 = compareKinds(mEdgeKind._1, searchEdgeKind._1)
      if (error1 >= 0 && error2 >= 0 && error1+error2<9){
        modelEdgeKind2Domain(mEdgeKind) += ((sEdgeIndex, error1+error2))
      }
    }
  }
  for (k <- modelEdgeKinds) modelEdgeKind2Domain(k) = modelEdgeKind2Domain(k).sortWith(_._2 < _._2)
  println(modelEdgeKind2Domain.map(x => (x._1, x._2.map(y => ((searchIndex2Node(searchIndex2Edge(y._1)._1), searchIndex2Node(searchIndex2Edge(y._1)._2), y._2))))).mkString("\n"))
  println

  println(modelIndex2EdgeKind)


  //Computation of modelEdgesContinuityCouples
  val modelEdgesContinuityCouples : Set[(Int, Int)] = Set()
  for ((mNode, mNeighbours) <- modelNodes){
    val mNeighboursList = mNeighbours.toList
    val nEdges = mNeighboursList.size
    for (i <- 0 until nEdges; j <- i+1 until nEdges){
      val edge1 = (min(mNode, mNeighboursList(i)), max(mNode, mNeighboursList(i)))
      val edge2 = (min(mNode, mNeighboursList(j)), max(mNode, mNeighboursList(j)))
      modelEdgesContinuityCouples += ((min(modelEdge2Index(edge1), modelEdge2Index(edge2)), max(modelEdge2Index(edge1), modelEdge2Index(edge2))))
    }
  }


  //Computation of searchEdgesContinuityCouples
  val searchEdgesContinuityCouples : Set[(Int, Int)] = Set()
  for ((sNode, sNeighbours) <- searchNodes){
    val sNeighboursList = sNeighbours.toList
    val nEdges = sNeighboursList.size
    for (i <- 0 until nEdges; j <- i+1 until nEdges){
      val edge1 = (min(sNode, sNeighboursList(i)), max(sNode, sNeighboursList(i)))
      val edge2 = (min(sNode, sNeighboursList(j)), max(sNode, sNeighboursList(j)))
      searchEdgesContinuityCouples += ((searchEdge2Index(edge1), searchEdge2Index(edge2)))
      searchEdgesContinuityCouples += ((searchEdge2Index(edge2), searchEdge2Index(edge1)))
    }
  }


  val variablesOrdering = modelEdgeKind2Index.toArray.sortBy(x => (x._2.size, modelEdgeKind2Domain(x._1).size)).map(_._2).flatten
  val startingVariables = variablesOrdering.take(5)
  println(variablesOrdering.mkString(" "))
  println







  //***---Declaring the VARIABLES---***
  val domain = -1 until nModelEdges
  val modelEdgeVars = for (i <- 0 until nModelEdges) yield CPIntVar(modelEdgeKind2Domain(modelIndex2EdgeKind(i)).map(_._1).toSeq:+(-1))
  val nViolationsAllowed = max((0.8*nModelEdges).toInt, nModelEdges-nSearchEdges)
  val valueOcc: Array[(Int, CPIntVar)] = (for (i <- -1 until nSearchEdges) yield {if (i == -1) (-1, CPIntVar(0 to nViolationsAllowed)) else (i, CPIntVar(0 to 1))}).toArray







  //***---Declaring the CONSTRAINTS---***
  add(gcc(modelEdgeVars, valueOcc))

  //Node continuity constraints
  val dummyXtoEachEdge = for (edgeIndex <- domain.toList) yield (-1, edgeIndex) //dummyXtoDummyY considered
  val eachEdgeToDummyY = for (edgeIndex <- domain.toList if edgeIndex != -1) yield (edgeIndex, -1)
  val tableValues = eachEdgeToDummyY ::: dummyXtoEachEdge ::: searchEdgesContinuityCouples.toList
  val tableValuesBis = eachEdgeToDummyY ::: dummyXtoEachEdge ::: (for (i <- 0 until nSearchEdges; j <- i+1 until nSearchEdges) yield (i,j)).toSet.--(searchEdgesContinuityCouples).toList
  for ((i, j) <- modelEdgesContinuityCouples)
    add(table(modelEdgeVars(i), modelEdgeVars(j), tableValues))
  println(tableValuesBis)
  for (i <- 0 until nModelEdges; j <- i+1 until nModelEdges)
    if (!modelEdgesContinuityCouples.contains((i,j)))
      add(table(modelEdgeVars(i), modelEdgeVars(j), tableValuesBis))







  //****----SEARCH BLOCK----****
  var lastVarBound = -1
  val rand = new Random
  search {
    val notBound = (0 until nModelEdges).filter(!modelEdgeVars(_).isBound).toSet
    if (notBound.isEmpty)
      noAlternative
    else if (notBound.size == nModelEdges) {
      lastVarBound = startingVariables(rand.nextInt(5))
      println("first variable selected " + lastVarBound + " : " + modelIndex2Edge(lastVarBound))
      val domainToBranch = modelEdgeKind2Domain(modelIndex2EdgeKind(lastVarBound)).filter(x => modelEdgeVars(lastVarBound).toArray.contains(x._1)).map(_._1)
      branchAll(domainToBranch)(value => add(modelEdgeVars(lastVarBound) == value))
    }
    else {
      assert(lastVarBound != -1)
      val A = modelIndex2Edge(lastVarBound)._1
      val B = modelIndex2Edge(lastVarBound)._2
      val neighboursFromA = modelNodes(A).map(x => modelEdge2Index((min(x, A), max(x, A))))
      val neighboursFromB = modelNodes(B).map(x => modelEdge2Index((min(x, B), max(x, B))))
      val neighbours = neighboursFromA union neighboursFromB
      val intersection = neighbours.intersect(notBound).toVector
      if (intersection.isEmpty) {
        val i = notBound.minBy(modelEdgeVars(_).size)
        lastVarBound = i
        val domainToBranch =modelEdgeKind2Domain(modelIndex2EdgeKind(lastVarBound)).filter(x => modelEdgeVars(lastVarBound).toArray.contains(x._1)).map(_._1)
        branchAll(domainToBranch)(v => add(modelEdgeVars(i)==v))
      }
      else {
        val i = intersection(rand.nextInt(intersection.size))
        lastVarBound = i
        val domainToBranch = modelEdgeKind2Domain(modelIndex2EdgeKind(lastVarBound)).filter(x => modelEdgeVars(lastVarBound).toArray.contains(x._1)).map(_._1)
        branchAll(domainToBranch)(v => add(modelEdgeVars(i)==v))
      }
    }
  }




  //****----ON SOLUTION BLOCK----*****
  var bestSol = Array.fill(nModelEdges)(-1)
  var bestVal = Int.MaxValue
  onSolution {
    bestSol = modelEdgeVars.map(_.value)
    bestVal = valueOcc(0)._2.value
  }




  //****----LAUNCH SEARCH----****
  val startTime = System.currentTimeMillis()

  //<<-- Minimize the number of unbound modelVars -->>
  minimize(valueOcc(0)._2)


  //<<-- Find a first solution for the LNS -->>
  try{
    start(nSols = 1)
    println("First sol found after " + (System.currentTimeMillis()-startTime) + " ms")
  }
  catch {
    case e : NoSolutionException => {
      println("No Solution")
      System.exit(0)
    }
  }


  //<<-- LNS LOOP -->>
  var fragmentSize = 50.0
  while (bestVal != 0 && System.currentTimeMillis()-startTime < 100*1000){
    val selectedVars : ListBuffer[Int] = ListBuffer()
    val stat = startSubjectTo(failureLimit = 350){
      (0 until nModelNodes).filter(k => !(rand.nextInt(100) < fragmentSize)).foreach(i => {
        add(modelEdgeVars(i) == bestSol(i))
        selectedVars += i
      })
      if (selectedVars.size > 0){
        lastVarBound = selectedVars(rand.nextInt(selectedVars.size))
        println("selectedVars is " + selectedVars.size + "/" + nModelEdges + " unbound " + (0 until nModelNodes).filter(!modelEdgeVars(_).isBound).toSet.size)
      }
      else {
        lastVarBound = -1
        println("NO VAR SELECTED !!!!!!")
        val notBound = (0 until nModelNodes).filter(!modelEdgeVars(_).isBound).toSet
        println(notBound.mkString(" "))
      }

    }

    if (stat.completed)
      fragmentSize = min(100, fragmentSize * 1.1)
    else
      fragmentSize = max(15, fragmentSize * .9)

    println("New fragmentSize is " + fragmentSize)
  }





  //<<-- Termination -->>
  val writer = new PrintWriter(new File("../copOut.txt" ))
  writer.write("{\n")
  val matching = edgeMatchingToNodeMatching(bestSol)
  val matchingSize = matching.size
  var i = 1
  for ((k, v) <- matching){
    writer.write(k + ":" + v)
    if (i!=matchingSize) writer.write(",\n")
    println(k + " : " + v)
    i+=1
  }
  writer.write("}")
  writer.close()

  println("\nSearch ended after " + (System.currentTimeMillis()-startTime)/1000 + "s")
  println("bestVal is " + bestVal)




  def readFile (fileName : String) : (  Map[(Int,Int), Int],
                                        Map[Int, (Int, Int)],
                                        Map[(Int,Int), Int],
                                        Map[Int, (Int, Int)] ,
                                        Map[Int, Set[Int]] ) = {
    val sc = new Scanner(new BufferedReader(new FileReader(fileName)))

    val points2index = Map[(Int,Int), Int]()
    val index2points = Map[Int, (Int,Int)]()
    val edge2index = Map[(Int, Int), Int]()
    val index2edge = Map[Int, (Int, Int)]()
    var nodes = Map[Int, Set[Int]]()

    var pIndex = 0
    var eIndex = 0
    val pattern = "\\(([0-9]+), ([0-9]+)\\) \\(([0-9]+), ([0-9]+)\\) .*".r

    while (sc.hasNext) {
      try {
        val line = sc.nextLine()
        val pattern(x1,x2,y1,y2) = line

        val x = (x1.toInt, x2.toInt)
        val y = (y1.toInt, y2.toInt)
        if (!points2index.contains(x)) {
          points2index.+=((x, pIndex))
          index2points.+=((pIndex, x))
          nodes+=((pIndex, Set()))
          pIndex+=1
        }
        if (!points2index.contains(y)) {
          points2index.+=((y, pIndex))
          index2points.+=((pIndex, y))
          nodes.+=((pIndex, Set()))
          pIndex+=1
        }

        val edge: (Int, Int) = (points2index(x), points2index(y))
        if (edge._1 < edge._2){
          edge2index(edge) = eIndex
          index2edge(eIndex) = edge
          eIndex += 1
        }
        
        nodes(points2index(x)) += points2index(y)
      }
      catch {
        case e: Exception =>
      }
    }

    (points2index, index2points, edge2index, index2edge, nodes)
  }

  def edgeMatchingToNodeMatching(edgeValues : Array[Int]) : Map[(Int, Int), (Int, Int)] = {
    val preMatching : Map[Int, ListBuffer[Int]]= Map()
    for (eIndex <- 0 until nModelEdges) preMatching(eIndex) = ListBuffer()
    for (eIndex <- 0 until nModelEdges){
      if (edgeValues(eIndex) != -1){
        val (modelFromIndex, modelToIndex) = modelIndex2Edge(eIndex)
        val (searchFromIndex, searchToIndex) = searchIndex2Edge(edgeValues(eIndex))
        preMatching(modelFromIndex) ++= Seq(searchFromIndex, searchToIndex)
        preMatching(modelToIndex) ++= Seq(searchFromIndex, searchToIndex)
      }
    }

    val matching : Map[(Int, Int), (Int, Int)] = Map()
    for (nIndex <- 0 until nModelNodes)
      if (preMatching(nIndex).nonEmpty) {
        matching(modelIndex2Node(nIndex)) = searchIndex2Node(preMatching(nIndex).maxBy(x => preMatching(nIndex).count(x == _)))
      }

    matching
  }
}


